#include <stdbool.h>
#include <assert.h>
#include <stdlib.h>
#include <stdint.h>

#include "tuple.h"


struct _tuple_t {

	vertex_t v; //index of the vertex
	tuple_t anc; // pointer to its ancestor
	u32 eps;   //epsilon
    bool dir;           // true if is forward, false if is backward
};


tuple_t tuple_from_vertex(vertex_t vertex, tuple_t ancestor, u32 capacity, bool direction)
{

	assert(vertex != NULL);
	tuple_t tuple = calloc(1,sizeof(struct _tuple_t));
	tuple->v = vertex; 
	tuple->anc = ancestor;
    if (tuple->anc != NULL){
	    tuple->eps = MIN(capacity, tuple->anc->eps);
    }else {
        tuple->eps = capacity;
    }
    tuple->dir = direction;

	return (tuple);
}
/*
 * Build a new tuple from the given vertex,its capacity and its ancestor,  *using references to them.
 *
 * Do NOT free vertex and ancestor after creating the tuple, but only through
 * tuple_destroy.
 *
 * PRE: 'vertex' and 'ancestor' must be not NULL.
 */


tuple_t tuple_destroy(tuple_t t)
{
	assert(t != NULL);
    t->v = NULL;
    t->anc = NULL;
	free(t);
	t = NULL;
	return(t);
}
  
/*
 * Free the memory allocated by given tuple.
 * Set 't' to NULL.
 *
 * PRE: 't' must be not NULL.
 */

vertex_t tuple_vertex(tuple_t t) 
{

	assert(t != NULL);
	return(t->v);
}
/*
 * Return a reference to the first tuple element.
 *
 * PRE: 't' must be not NULL.
 */

tuple_t tuple_anc(tuple_t t)
{

	assert(t != NULL);
	tuple_t ancestor = NULL;
	ancestor = t->anc;
	return(ancestor);
}
/*
 * Return a reference to the second tuple element.
 *
 * PRE: 't' must be not NULL.
 */

u32 tuple_eps(tuple_t t){
	
    assert(t != NULL);
	u32 epsilon = t->eps;
	return(epsilon);
}
/*
 * Return a reference to the third tuple element.
 *
 * PRE: 't' must be not NULL.
 */

bool tuple_dir(tuple_t t){
	
    assert(t != NULL);
	bool direction = t->dir;
	return(direction);
}
/*
 * Return a reference to the fourth tuple element.
 *
 * PRE: 't' must be not NULL.
 */

bool tuple_has_anc(tuple_t t){
    assert(t != NULL);
    return (tuple_anc(t) != NULL);
}
