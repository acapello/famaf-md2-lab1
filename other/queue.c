#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "queue.h"

typedef struct _node_t *node_t;

struct _node_t {
	void* data;
	node_t next_node;
};

struct _queue_t {
    unsigned int counter;
    node_t head;
	node_t front;
	node_t last;
};

queue_t queue_new(void) {
    queue_t new_queue = calloc(1, sizeof(struct _queue_t));
	new_queue->counter = 0;
	new_queue->head = NULL;
	new_queue->front = NULL;
	new_queue->last = NULL;

	assert(new_queue != NULL);
	return(new_queue);
}

bool queue_is_empty(queue_t queue) {
    assert(queue != NULL);
    return(queue->front == NULL);
}

unsigned int queue_length(queue_t queue) {
    assert(queue != NULL);
    return(queue->counter);
}

void queue_enqueue(queue_t queue, void* data) {
	node_t new_node = NULL;

	assert(queue != NULL);

	new_node = calloc(1, sizeof(struct _node_t));
	new_node->data = data;
	new_node->next_node = NULL;

    if(queue->last == NULL) {
	    queue->front = new_node;
	    queue->head = new_node;
	    queue->last = new_node;
	} else {
	    queue->last->next_node = new_node;
        queue->last = new_node;
        if (queue->head == NULL)
            queue->head = new_node;
	}

	(queue->counter)++;
}


void queue_next(queue_t queue) {
    assert(queue != NULL);
    if(queue->head != NULL)
        queue->head = queue->head->next_node;
}

void queue_dequeue(queue_t queue, void free_function()) {
    node_t node = NULL;

    assert(queue != NULL);
    node = queue->front;

    if(queue->front == queue->last) {
        queue->head = NULL;
        queue->front = NULL;
        queue->last = NULL;
    } else {
        queue->front = queue->front->next_node;
    }

    (queue->counter)--;
    free_function(node->data);
    free(node);
}

void* queue_head(queue_t queue) {
    assert(queue != NULL);
    assert(queue->head != NULL);

    return(queue->head->data);
}

void* queue_front(queue_t queue) {
    assert(queue != NULL);
    assert(queue->counter > 0);

    return(queue->front->data);
}

void* queue_last(queue_t queue) {
    assert(queue != NULL);
    assert(queue->last != NULL);
    
    return(queue->last->data);
}

bool queue_is_there(queue_t queue, void* data, bool compare_function()) {
    node_t aux_point = NULL;
    bool is_there;

    assert(queue != NULL);

    aux_point = queue->front;
    is_there = false;

    while (aux_point != NULL && !is_there) {
        is_there = compare_function(data, aux_point->data);
        aux_point = aux_point->next_node;
    }

    return(is_there);

}

bool queue_is_done(queue_t queue) {
    return(queue->head == NULL);
}

void queue_free_full(queue_t queue, void free_function()) {
    assert(queue != NULL);

    while (!queue_is_empty(queue))
        queue_dequeue(queue, free_function);

    free(queue);
    queue = NULL;
}

void queue_dump(queue_t queue, void print_function()) {
    node_t aux_point = NULL;

    assert(queue != NULL);

    aux_point = queue->front;

    while (aux_point != NULL) {
        assert(aux_point->data != NULL);
        print_function(aux_point->data);
        aux_point = aux_point->next_node;
    }
}
