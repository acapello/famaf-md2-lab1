#ifndef _QUEUE_H
#define _QUEUE_H

#include <stdbool.h>


typedef struct _queue_t *queue_t;


queue_t queue_new(void);
/*
 * Return a newly created, empty queue.
 *
 * The caller must call queue_free_full when done using the resulting queue,
 * so the resources allocated by this call are freed.
 *
 * POST: the result will not be NULL, and queue_length(result) will be 0.
 */

bool queue_is_empty(queue_t queue);
/*
 * Returns true if queue is empty or false if it isn't.
 *
 */

unsigned int queue_length(queue_t queue);
/*
 * Return the number of elements in the given 'queue'.
 * Constant order complexity.
 *
 * PRE: 'queue' must not be NULL.
 */

void queue_enqueue(queue_t queue, void* data);
/*
 * After calling this procedure, the new queue is equals to 'queue' 
 * with the 'data' enqueued.
 *
 * The given 'data' are inserted in the list, so do not destroy them.
 *
 * PRE: all 'queue' and 'data' must be not NULL.
 *
 * POST: the length of the result will be the same as the length of 'queue'
 * plus one. The elements of the result will be the same as the one in 'queue'
 * with 'data' added. The resulting queue can have equals elements.
 */

void queue_next(queue_t queue);
/*
 * After calling this procedure, the head element of the new queue will be the
 * next element of the head in 'queue'.
 * If the head element of 'queue' is the last one, the head of the new
 * queue will be NULL.
 *
 * The head element of 'queue' keeps in the memory of the ADT like used 
 * or older elements.
 * 
 * Note that there is a difference between the head element and the front
 * element. The head element is the next to be served, and the front element
 * is the first-time added element if there wasn't a call to queue_dequeue().
 *
 * PRE: 'queue' must not be NULL.
 *
 * POST: the length of the result will be the same as the length of 'queue'. 
 * The elements of the result will be the same as the one in 'queue'.
 * 
 * 
 */
 
void queue_dequeue(queue_t queue, void free_function());
/*
 * After calling this procedure, the new queue is equals to 'queue' 
 * with the front element removed.
 *
 * free_function is a function that free the memory allocated by
 * the elements of the queue.
 *
 * PRE: 'queue' must not be NULL.
 *
 * POST: the length of the result will be the same as the length of 'queue'
 * minus one. The elements of the result will be
 * the same as the one in 'list' with the entry for'word' removed.
 */
 
void* queue_head(queue_t queue);
/*
 * Returns the head element in the queue, i.e., the next element to
 * be served.
 *
 * PRE: 'queue' must not be NULL.
 *
 */

void* queue_front(queue_t queue);
/*
 * Returns the front element in the queue.
 *
 * PRE: 'queue' must not be NULL.
 *
 */

void* queue_last(queue_t queue);
/*
 * Returns the last element in the queue.
 *
 * PRE: 'queue' must not be NULL.
 *
 */

bool queue_is_there(queue_t queue, void* data, bool compare_function());
/*
 * Returns true if 'data' is in 'queue' and false if it isn't.
 * 
 * 'compare_function()' is a function that you provide
 * to compare two elements in the queue.
 * Your function must be like: bool function_name(void* elem1, void* elem2){...}
 *
 * PRE: 'queue' must not be NULL.
*/

bool queue_is_done(queue_t queue);
/*
 * Returns true if there is no more elements to serve in the queue
 * and false if it isn't.
 * Note that there is a difference with queue_is_empty. This function
 * returns true when queue_head() is NULL.
 *
 * PRE: 'queue' must not be NULL.
*/

void queue_free_full(queue_t queue, void free_function());
/*
 * Free the resources allocated for the given 'queue', and set it to NULL.
 * All the elements in the queue will be freed also.
 * 
 * PRE: 'queue' must not be NULL.
 */

void queue_dump(queue_t queue, void print_function());
/*
 * Dump the given 'queue'.
 *
 * 'print_function()' is a function that you provide to print a element 
 * in 'queue'.
 *
 * PRE: 'queue' must not be NULL.
 */

#endif
