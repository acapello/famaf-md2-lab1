#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "API.h"

/**
 * Where 's' is the output of DondeEstamosParados().
 */
static bool is_maximal(int s)
{
    return s >= 100;
}

/**
 * Where 's' is the output of DondeEstamosParados().
 */
static bool is_aug(int s)
{
    return s >= 100 ? is_aug(s - 100) : s >= 10;
}

int main()
{
    DragonP d = NuevoDragon();

    if (LlenarDragon(d) != 1) {
        fprintf(stderr, "The Dovahkiin sleeps.\n");
        DestruirDragon(d);
        return 1;
    }

    while (ECAML(d) == 1)
        AumentarFlujoYtambienImprimirCamino(d);
       /* AumentarFlujo(d);*/

    ImprimirFlujo(d);
    ImprimirValorFlujo(d);
    ImprimirCorte(d);

    DestruirDragon(d);

    return 0;
}
