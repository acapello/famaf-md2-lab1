#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#define F(N, T, V)     \
    T var##N = V;

#define G(N, T, V)     \
    T fun##N(void) {   \
        F(N, T, V)     \
        return var##N; \
    }

F(INT, int, 12)
G(INT, int, 32)

struct pair {
    int first;
    int second;
};

struct entry {
    struct pair key;
    struct pair val;
    int x;
};

/*
#define IMPLEMENT_VECTOR_POD(NAME, DEFAULT_VALUE) \
    static const struct entry kVectorPodDefaultValue_##NAME = DEFAULT_VALUE;

#define ENTRY_INITIALIZER(KEY, VALUE) { KEY, VALUE, 20 }

#define IMPLEMENT_HASHMAP_POD(NAME, DEFAULT_KEY, DEFAULT_VALUE) \
    IMPLEMENT_VECTOR_POD(NAME, ENTRY_INITIALIZER((DEFAULT_KEY), (DEFAULT_VALUE)))

#define DEF_KEY {0,0}
#define DEF_VAL {2,2}

IMPLEMENT_HASHMAP_POD(blah, DEF_KEY, DEF_VAL)
*/

#define IMPLEMENT_VECTOR_POD(NAME, DEFAULT_VALUE) \
    static const struct entry kVectorPodDefaultValue_##NAME = DEFAULT_VALUE;

#define KLUGE(...) __VA_ARGS__

#define IMPLEMENT_HASHMAP_POD(NAME, DEFAULT_KEY, DEFAULT_VALUE) \
    IMPLEMENT_VECTOR_POD(NAME, KLUGE({DEFAULT_KEY, DEFAULT_VALUE, 20}))

IMPLEMENT_HASHMAP_POD(blah, KLUGE({0,0}), KLUGE({2,2}))

struct non { int :0; };

int main(void)
{
    printf("%d %d\n", varINT, funINT());
    printf("%ld\n", sizeof(struct non));

    struct non* asd = malloc(sizeof(struct non));
    (void)asd;
    printf("%p\n", asd);

    struct non zxc;
    (void)zxc;
    asd = &zxc;
    printf("%p\n", asd);

    return 0;
}
