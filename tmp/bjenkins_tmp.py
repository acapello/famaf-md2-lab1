"""
Decimal. Same as H,h, but decimal instead of hexidecimal. Did I mention this is good for switch statement optimization? For example, the perfect hash for 1,16,256 is hash=((key+(key>>3))&3); and the perfect hash for 1,2,3,4,5,6,7,8 is hash=(key&7); and the perfect hash for 1,4,9,16,25,36,49 is

  ub1 tab[] = {0,7,0,2,3,0,3,0};
  hash = key^tab[(key<<26)>>29];
"""

"""
k(d   k(b           >> 3           + k            & 3
1     0 0000 0001   0 0000 0000    0 0000 0001    0 0000 0001    1
16    0 0001 0000   0 0000 0010    0 0001 0010    0 0000 0010    2
256   1 0000 0000   0 0010 0000    1 0010 0000    0 0000 0000    0

not monotone
"""

keys = [1,16,256]
hashes = []

for k in keys:
    h = (k + (k>>3)) & 3
    hashes.append(h)

for k,h in zip(keys, hashes):
    print k, "=>", h

"""
1    00 0001
4    00 0100
9    00 1001
16   01 0000
25   01 1001
36   10 0100
49   11 0001
"""

keys = [1,4,9,16,25,36,49]
hashes = []
tab = [0,7,0,2,3,0,3,0]

for k in keys:
    t = k << 26
    t = t >> 29
    h = k ^ tab[t]
    hashes.append(h)

for k,h in zip(keys, hashes):
    print k, "=>", h
