#include <iostream>
#include <functional>
#include <sstream>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

struct TrivialHash : unary_function<unsigned, size_t> {
    size_t mod_;
    TrivialHash(size_t m) : mod_(m) { }
    size_t operator()(unsigned n) {
        return n % mod_;
    }
};

struct FNVHash : unary_function<unsigned, size_t> {
    size_t mod_;
    FNVHash(size_t m) : mod_(m) { }
    size_t operator()(unsigned n) {
        size_t res = 14695981039346656037ULL;
        res ^= n & 0xFF;
        res *= 1099511628211ULL;
        res ^= (n>>8) & 0xFF;
        res *= 1099511628211ULL;
        res ^= (n>>16) & 0xFF;
        res *= 1099511628211ULL;
        res ^= (n>>24) & 0xFF;
        res *= 1099511628211ULL;
        return res % mod_;
    }
};

struct DJBX33AHash : unary_function<unsigned, size_t> {
    size_t mod_;
    DJBX33AHash(size_t m) : mod_(m) { }
    size_t operator()(unsigned n) {
        size_t res = (n>>24) & 0xFF;
        res = res*33 + ((n>>16) & 0xFF);
        res = res*33 + ((n>>8) & 0xFF);
        res = res*33 + (n & 0xFF);
        return res % mod_;
    }
};

struct ScaleHash : unary_function<unsigned, size_t> {
    size_t mod_;
    size_t first_;
    size_t span_;
    ScaleHash(size_t m, size_t f, size_t l)
        : mod_(m), first_(f), span_(l-f) { }
    size_t operator()(unsigned n) {
        return float(n-first_)/span_*mod_;
    }
};

int main(int argc, char** argv)
{
    if (argc < 2) {
        cout << "Bad argument count.\n";
        return 1;
    }

    stringstream arg(argv[1]);
    size_t bucket_count = 0;
    arg >> bucket_count;
    if (!arg || bucket_count == 0) {
        cout << "Bad argument.\n";
        return 1;
    }

    vector<unsigned> values;
    values.reserve(100000);

    while (true) {
        unsigned number;
        cin >> number;

        if (!cin)
            break;

        values.push_back(number);
    }

    if (values.size() == 0) {
        cout << "Empty input. \n";
        return 0;
    }

    //TrivialHash hasher(bucket_count);
    //FNVHash hasher(bucket_count);
    DJBX33AHash hasher(bucket_count);
    //ScaleHash hasher(bucket_count, values[0], values[values.size()-1]);

    map<size_t, size_t> buckets;
    for (auto& i : values)
        ++buckets[hasher(i)];

    typedef pair<size_t, size_t> MPair;

    cout << "Buckets: " << bucket_count << "\n";
    cout << "\tUsed: " << buckets.size() << "\t" << float(buckets.size())/bucket_count*100 << "%\n";
    cout << "\tWasted: " << bucket_count - buckets.size() << "\t" << (1 - float(buckets.size())/bucket_count)*100 << "%\n";
    cout << "Average bucket size: " << float(values.size())/buckets.size() << "\n";
    cout << "Table occupancy ratio: " << float(values.size())/bucket_count << "\n";

    size_t max_bucket_size = 0;
    //auto max_it = max_element(buckets.begin(), buckets.end(), [=](const MPair& p1, const MPair& p2) {
    //    return p1.second < p2.second;
    //});
    //if (max_it != buckets.end())
    //    max_bucket_size = max_it->second;

    map<size_t, size_t> histogram;

    for (auto &i : buckets) {
        if (i.second > max_bucket_size)
            max_bucket_size = i.second;
        ++histogram[i.second];
    }

    cout << "Max bucket size: " << max_bucket_size << "\n";

    for (auto &i : histogram)
        cout << "Buckets with " << i.first << ": " << i.second << "\t" << float(i.second)/buckets.size()*100 << "%\n";

    return 0;
}
