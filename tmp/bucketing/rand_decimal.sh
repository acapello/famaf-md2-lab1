rand_decimal() {
    n=$1  # amount of numbers to generate

    shift

    b=4  # by default, generate 4-byte numbers
    if [ $# -gt 0 ]; then
        b=$1  
    fi

    for ((i=0; $i<$n; i=$((i+1)) )); do
        dd if=/dev/urandom count=$b bs=1 2>/dev/null | od -l | head -n1 | sed -r 's/.+ +([0-9]+)/\1/'
    done
}
