
/* declarations of external names */

void a_f(int i);

void a_s(const char* s);

/* local definitions */

static char* b_f(int i)
{
    a_f(i);

    return "b_f(i)";
}


int main()
{
    a_s(b_f(12));

    return 0;
}
