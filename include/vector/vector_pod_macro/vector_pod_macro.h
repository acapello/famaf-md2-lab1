/*
 * vector_pod_macro.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _VECTOR_POD_MACRO_H_
#define _VECTOR_POD_MACRO_H_

#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>

#include <vector/vector_common.h>

/*** MACHINE-GENERATED ***/

/*** CODE BEGIN ***/

/*** DECLARATION MACRO BEGIN ***/

#define NOQUALIF

#define DECLARE_VECTOR_POD(NAME, value_type)                                   \
	__DECLARE_VECTOR_POD(NAME, value_type, NOQUALIF)

#define DECLARE_STATIC_VECTOR_POD(NAME, value_type)                            \
	__DECLARE_VECTOR_POD(NAME, value_type, static)

#define __DECLARE_VECTOR_POD(NAME, value_type, QUALIF)                         \
                                                                               \
/**                                                                            \
 * This is a specialization of 'vector' intended to be used with POD elements. \
 * As such, contained element are not 'constructed' nor 'destroyed', and       \
 * copying is performed by simple byte mirroring.                              \
 * When needed, for instance when resizing, a user-configured 'default'        \
 * element is used.                                                            \
 */                                                                            \
                                                                               \
/**                                                                            \
 * Callback function used when iterating over the vector, where,               \
 *   i      The elements' index.                                               \
 *   v      The elements' value.                                               \
 */                                                                            \
typedef void (*vector_##NAME##_foreach_f)(size_t i, value_type* v);            \
                                                                               \
/**                                                                            \
 * Callback function used when iterating over the vector, where,               \
 *   i      The elements' index.                                               \
 *   v      The elements' value.                                               \
 *   d      Opaque object pointer reserved for the user's contextual data.     \
 */                                                                            \
typedef void (*vector_##NAME##_foreach_data_f)(size_t i, value_type* v, void* d); \
                                                                               \
/**                                                                            \
 * Callback function used when iterating over the vector, where,               \
 *   i      The elements' index.                                               \
 *   v      The elements' value.                                               \
 *   f      Opaque function pointer reserved for the user's contextual data.   \
 */                                                                            \
typedef void (*vector_##NAME##_foreach_func_f)(size_t i, value_type* v, void(*f)()); \
                                                                               \
typedef struct vector_##NAME* vector_##NAME##_t;                               \
                                                                               \
/**                                                                            \
 * Constructor.                                                                \
 * Construct a vector with initial capacity 'capacity'.                        \
 * The capacity of a vector is the maximum amount of elements that can be added \
 * with the certainty that the vector will not incur in reallocs. This, in     \
 * turn, assures that pointers into the array will not be invalidated.         \
 *                                                                             \
 * Post: vector_##NAME##_create(capacity) != NULL                              \
 */                                                                            \
QUALIF vector_##NAME##_t vector_##NAME##_create(size_t capacity);              \
                                                                               \
/**                                                                            \
 * Copy constructor.                                                           \
 *                                                                             \
 * Pre: vec != NULL                                                            \
 * Post: vector_##NAME##_copy(vec) != NULL                                     \
 */                                                                            \
QUALIF vector_##NAME##_t vector_##NAME##_copy(vector_##NAME##_t vec);          \
                                                                               \
/**                                                                            \
 * Destructor.                                                                 \
 *                                                                             \
 * Pre: vec != NULL                                                            \
 */                                                                            \
QUALIF void vector_##NAME##_destroy(vector_##NAME##_t vec);                    \
                                                                               \
/**                                                                            \
 * Return the vector's grow factor.                                            \
 * When the vector needs to grow, and the current grow policy is VGP_PREALLOC, \
 * it does so by this factor.                                                  \
 *                                                                             \
 * Pre: vec != NULL && vector_##NAME##_get_grow_factor(vec) > 1                \
 */                                                                            \
QUALIF float vector_##NAME##_get_grow_factor(vector_##NAME##_t vec);           \
                                                                               \
/**                                                                            \
 * Set the vector's grow factor.                                               \
 * When the vector needs to grow, and the current grow policy is VGP_PREALLOC, \
 * it does so by this factor.                                                  \
 *                                                                             \
 * Pre: vec != NULL && grow_factor > 1                                         \
 */                                                                            \
QUALIF void vector_##NAME##_set_grow_factor(vector_##NAME##_t vec, float grow_factor); \
                                                                               \
/**                                                                            \
 * Return the vector's grow policy.                                            \
 * When the vector needs to grow it does so according to the current policy:   \
 *   VGP_ADHOC      The vector grows one element at a time.                    \
 *   VGP_PREALLOC   The vector grows by a configurable factor of its current   \
 *                  size.                                                      \
 *                                                                             \
 * Pre: vec != NULL                                                            \
 */                                                                            \
QUALIF vgpolicy_t vector_##NAME##_get_grow_policy(vector_##NAME##_t vec);      \
                                                                               \
/**                                                                            \
 * Set the vector's grow policy.                                               \
 * When the vector needs to grow it does so according to the current policy:   \
 *   VGP_ADHOC      The vector grows one element at a time.                    \
 *   VGP_PREALLOC   The vector grows by a configurable factor of its current   \
 *                  size.                                                      \
 *                                                                             \
 * Pre: vec != NULL && grow_policy & VGP_MASK                                  \
 */                                                                            \
QUALIF void vector_##NAME##_set_grow_policy(vector_##NAME##_t vec, vgpolicy_t grow_policy); \
                                                                               \
/**                                                                            \
 * Append 'val' to the end of the vector, resizing it if necessary.            \
 * Return a poiter to the appended element.                                    \
 *                                                                             \
 * Pre: vec != NULL                                                            \
 * Post: vector_##NAME##_push_back(vec, val) != NULL                           \
 */                                                                            \
QUALIF value_type* vector_##NAME##_push_back(vector_##NAME##_t vec, value_type val); \
                                                                               \
/**                                                                            \
 * Remove the last element from vector.                                        \
 * The removed element is destructed.                                          \
 *                                                                             \
 * Pre: vec != NULL && vector_##NAME##_size(vec) > 0                           \
 */                                                                            \
QUALIF void vector_##NAME##_pop_back(vector_##NAME##_t vec);                   \
                                                                               \
/**                                                                            \
 * Return a pointer to the last element in the vector.                         \
 *                                                                             \
 * Pre: vec != NULL && vector_size(vec) > 0                                    \
 * Post: vector_##NAME##_back(vec) != NULL                                     \
 */                                                                            \
QUALIF value_type* vector_##NAME##_back(vector_##NAME##_t vec);                \
                                                                               \
/**                                                                            \
 * Return the vector's current size.                                           \
 *                                                                             \
 * Pre: vec != NULL                                                            \
 */                                                                            \
QUALIF size_t vector_##NAME##_size(vector_##NAME##_t vec);                     \
                                                                               \
/**                                                                            \
 * Return the vector's current capacity.                                       \
 * The capacity of a vector is the maximum amount of elements that can be added \
 * with the certainty that the vector will not incur in reallocs. This, in     \
 * turn, assures that pointers into the array will not be invalidated.         \
 *                                                                             \
 * Pre: vec != NULL                                                            \
 */                                                                            \
QUALIF size_t vector_##NAME##_capacity(vector_##NAME##_t vec);                 \
                                                                               \
/**                                                                            \
 * Return a pointer to the element with position 'index'.                      \
 *                                                                             \
 * Pre: vec != NULL && index < vector_##NAME##_size(vec)                       \
 * Post: vector_##NAME##_at(vec, index) != NULL                                \
 */                                                                            \
QUALIF value_type* vector_##NAME##_at(vector_##NAME##_t vec, size_t index);    \
                                                                               \
/**                                                                            \
 * Call 'f' for each element in the vector, in order, from lower to higher     \
 * index.                                                                      \
 *                                                                             \
 * Pre: vec != NULL && f != NULL                                               \
 */                                                                            \
QUALIF void vector_##NAME##_foreach(vector_##NAME##_t vec, vector_##NAME##_foreach_f f); \
                                                                               \
/**                                                                            \
 * Call 'f' for each element in the vector, in order, from lower to higher     \
 * index. Pass 'data' along to 'f'.                                            \
 *                                                                             \
 * Pre: vec != NULL && f != NULL                                               \
 */                                                                            \
void vector_##NAME##_foreach_data(vector_##NAME##_t vec, vector_##NAME##_foreach_data_f f, \
        void* data);                                                           \
                                                                               \
/**                                                                            \
 * Call 'f' for each element in the vector, in order, from lower to higher     \
 * index. Pass 'data' along to 'f'.                                            \
 *                                                                             \
 * Pre: vec != NULL && f != NULL                                               \
 */                                                                            \
void vector_##NAME##_foreach_func(vector_##NAME##_t vec, vector_##NAME##_foreach_func_f f, \
        void(*func)());                                                        \
                                                                               \
/**                                                                            \
 * Change the size of the vector to 'new_size'.                                \
 * If the size is decreased, elements are removed from the end.                \
 * If the size is increased, default elements are appended.                    \
 * Note that increasing the size can invalidate pointers into the vector.      \
 *                                                                             \
 * Pre: vec != NULL                                                            \
 * Post: vector_##NAME##_size(vec) == new_size                                 \
 */                                                                            \
QUALIF void vector_##NAME##_resize(vector_##NAME##_t vec, size_t new_size);    \
                                                                               \
/**                                                                            \
 * Change the capacity of the vector to 'new_capacity'.                        \
 * This function can only increase the capacity.                               \
 * The capacity of a vector is the maximum amount of elements that can be added \
 * with the certainty that the vector will not incur in reallocs. This, in     \
 * turn, assures that pointers into the array will not be invalidated.         \
 * Note that increasing the capacity can invalidate pointers into the vector.  \
 *                                                                             \
 * Pre: vec != NULL                                                            \
 * Post: vector_##NAME##_capacity(vec) == new_capacity                         \
 */                                                                            \
QUALIF void vector_##NAME##_reserve(vector_##NAME##_t vec, size_t new_capacity); \
                                                                               \
/**                                                                            \
 * Remove from the vector the element at position 'index'.                     \
 * Note that is potentially a costly operation.                                \
 *                                                                             \
 * Pre: vec != NULL && index < vector_##NAME##_size(index)                     \
 */                                                                            \
QUALIF void vector_##NAME##_erase(vector_##NAME##_t vec, size_t index);        \
                                                                               \
/**                                                                            \
 * Remove all elements from the vector.                                        \
 * Note that this does not reduce the vector's capacity.                       \
 *                                                                             \
 * Pre: vec != NULL                                                            \
 * Post: vector_##NAME##_size(vec) == 0                                        \
 */                                                                            \
QUALIF void vector_##NAME##_clear(vector_##NAME##_t vec);                      \
                                                                               \
/**                                                                            \
 * Decrease the vector's capacity.                                             \
 * The amount of memory waived back in this manner depends on the current grow \
 * policy.                                                                     \
 *                                                                             \
 * Pre: vec != NULL                                                            \
 */                                                                            \
QUALIF void vector_##NAME##_fit(vector_##NAME##_t vec);                        \

/*** DECLARATION MACRO END ***/

/*** IMPLEMENTATION MACRO BEGIN ***/

#define IMPLEMENT_VECTOR_POD(NAME, value_type, DEFAULT_GROW_POLICY, DEFAULT_GROW_FACTOR, DEFAULT_VALUE) \
                                                                               \
struct vector_##NAME {                                                         \
    size_t capacity;                                                           \
    size_t size;                                                               \
    vgpolicy_t grow_policy;                                                    \
    float grow_factor;                                                         \
    value_type* arr;                                                           \
};                                                                             \
                                                                               \
static const vgpolicy_t kVectorPodDefaultGrowPolicy_##NAME = DEFAULT_GROW_POLICY; \
static const float kVectorPodDefaultGrowFactor_##NAME = DEFAULT_GROW_FACTOR;   \
static const value_type kVectorPodDefaultValue_##NAME = DEFAULT_VALUE;         \
                                                                               \
static void vector_##NAME##_grow(vector_##NAME##_t vec)                        \
{                                                                              \
    assert(vec != NULL);                                                       \
                                                                               \
    switch (vec->grow_policy) {                                                \
        case VGP_PREALLOC:                                                     \
            vector_##NAME##_reserve(vec, vec->capacity*vec->grow_factor);      \
            break;                                                             \
        case VGP_ADHOC:                                                        \
            vector_##NAME##_reserve(vec, vec->capacity + 1);                   \
            break;                                                             \
        default:                                                               \
            assert(false);                                                     \
    }                                                                          \
}                                                                              \
                                                                               \
static void vector_##NAME##_shrink(vector_##NAME##_t vec)                      \
{                                                                              \
    assert(vec != NULL);                                                       \
                                                                               \
    switch (vec->grow_policy) {                                                \
        case VGP_PREALLOC:                                                     \
            vector_##NAME##_reserve(vec, vec->size*vec->grow_factor);          \
            break;                                                             \
        case VGP_ADHOC:                                                        \
            vector_##NAME##_reserve(vec, vec->size);                           \
            break;                                                             \
        default:                                                               \
            assert(false);                                                     \
    }                                                                          \
}                                                                              \
                                                                               \
vector_##NAME##_t vector_##NAME##_create(size_t capacity)                      \
{                                                                              \
    vector_##NAME##_t vec = NULL;                                              \
                                                                               \
    vec = malloc(sizeof(*vec));                                                \
    assert(vec != NULL);                                                       \
                                                                               \
    vec->arr = malloc(capacity*sizeof(value_type));                            \
    assert(vec->arr != NULL || capacity == 0);                                 \
                                                                               \
    vec->capacity = capacity;                                                  \
    vec->size = 0;                                                             \
    vec->grow_policy = kVectorPodDefaultGrowPolicy_##NAME;                     \
    vec->grow_factor = kVectorPodDefaultGrowFactor_##NAME;                     \
                                                                               \
    return vec;                                                                \
}                                                                              \
                                                                               \
vector_##NAME##_t vector_##NAME##_copy(vector_##NAME##_t vec)                  \
{                                                                              \
    vector_##NAME##_t copy = NULL;                                             \
    size_t i = 0;                                                              \
                                                                               \
    assert(vec != NULL);                                                       \
                                                                               \
    copy = vector_##NAME##_create(vec->capacity);                              \
                                                                               \
    copy->grow_policy = vec->grow_policy;                                      \
    copy->grow_factor = vec->grow_factor;                                      \
    for (i = 0; i < vec->size; ++i)                                            \
        copy->arr[i] = vec->arr[i];                                            \
    copy->size = vec->size;                                                    \
                                                                               \
    return copy;                                                               \
}                                                                              \
                                                                               \
void vector_##NAME##_destroy(vector_##NAME##_t vec)                            \
{                                                                              \
    assert(vec != NULL);                                                       \
                                                                               \
    free(vec->arr);                                                            \
    free(vec);                                                                 \
}                                                                              \
                                                                               \
float vector_##NAME##_get_grow_factor(vector_##NAME##_t vec)                   \
{                                                                              \
    assert(vec != NULL);                                                       \
                                                                               \
    return vec->grow_factor;                                                   \
}                                                                              \
                                                                               \
void vector_##NAME##_set_grow_factor(vector_##NAME##_t vec, float grow_factor) \
{                                                                              \
    assert(vec != NULL);                                                       \
    assert(grow_factor > 1);                                                   \
                                                                               \
    vec->grow_factor = grow_factor;                                            \
}                                                                              \
                                                                               \
vgpolicy_t vector_##NAME##_get_grow_policy(vector_##NAME##_t vec)              \
{                                                                              \
    assert(vec != NULL);                                                       \
                                                                               \
    return vec->grow_policy;                                                   \
}                                                                              \
                                                                               \
void vector_##NAME##_set_grow_policy(vector_##NAME##_t vec, vgpolicy_t grow_policy) \
{                                                                              \
    assert(vec != NULL);                                                       \
    assert(grow_policy == VGP_ADHOC || grow_policy == VGP_PREALLOC);           \
                                                                               \
    vec->grow_policy = grow_policy & VGP_MASK;                                 \
}                                                                              \
                                                                               \
value_type* vector_##NAME##_push_back(vector_##NAME##_t vec, value_type val)   \
{                                                                              \
    size_t pos = 0;                                                            \
                                                                               \
    assert(vec != NULL);                                                       \
                                                                               \
    if (vec->size == vec->capacity)                                            \
        vector_##NAME##_grow(vec);                                             \
                                                                               \
    pos = vec->size++;                                                         \
    vec->arr[pos] = val;                                                       \
    return vec->arr + pos;                                                     \
}                                                                              \
                                                                               \
void vector_##NAME##_pop_back(vector_##NAME##_t vec)                           \
{                                                                              \
    assert(vec != NULL);                                                       \
    assert(vec->size > 0);                                                     \
                                                                               \
    --vec->size;                                                               \
}                                                                              \
                                                                               \
value_type* vector_##NAME##_back(vector_##NAME##_t vec)                        \
{                                                                              \
    assert(vec != NULL);                                                       \
    assert(vec->size > 0);                                                     \
                                                                               \
    return vec->arr + (vec->size - 1);                                         \
}                                                                              \
                                                                               \
size_t vector_##NAME##_size(vector_##NAME##_t vec)                             \
{                                                                              \
    assert(vec != NULL);                                                       \
                                                                               \
    return vec->size;                                                          \
}                                                                              \
                                                                               \
size_t vector_##NAME##_capacity(vector_##NAME##_t vec)                         \
{                                                                              \
    assert(vec != NULL);                                                       \
                                                                               \
    return vec->capacity;                                                      \
}                                                                              \
                                                                               \
value_type* vector_##NAME##_at(vector_##NAME##_t vec, size_t index)            \
{                                                                              \
    assert(vec != NULL);                                                       \
    assert(index < vector_##NAME##_size(vec));                                 \
                                                                               \
    return vec->arr + index;                                                   \
}                                                                              \
                                                                               \
void vector_##NAME##_foreach(vector_##NAME##_t vec, vector_##NAME##_foreach_f f) \
{                                                                              \
    size_t i = 0;                                                              \
                                                                               \
    assert(vec != NULL);                                                       \
    assert(f != NULL);                                                         \
                                                                               \
    for (i = 0; i < vec->size; ++i)                                            \
        f(i, vec->arr + i);                                                    \
}                                                                              \
                                                                               \
void vector_##NAME##_foreach_data(vector_##NAME##_t vec, vector_##NAME##_foreach_data_f f, \
        void* data)                                                            \
{                                                                              \
    size_t i = 0;                                                              \
                                                                               \
    assert(vec != NULL);                                                       \
    assert(f != NULL);                                                         \
                                                                               \
    for (i = 0; i < vec->size; ++i)                                            \
        f(i, vec->arr + i, data);                                              \
}                                                                              \
                                                                               \
void vector_##NAME##_foreach_func(vector_##NAME##_t vec, vector_##NAME##_foreach_func_f f, \
        void(*func)())                                                         \
{                                                                              \
    size_t i = 0;                                                              \
                                                                               \
    assert(vec != NULL);                                                       \
    assert(f != NULL);                                                         \
                                                                               \
    for (i = 0; i < vec->size; ++i)                                            \
        f(i, vec->arr + i, func);                                              \
}                                                                              \
                                                                               \
void vector_##NAME##_resize(vector_##NAME##_t vec, size_t new_size)            \
{                                                                              \
    assert(vec != NULL);                                                       \
                                                                               \
    if (new_size > vec->capacity)                                              \
        vector_##NAME##_reserve(vec, new_size);                                \
                                                                               \
    while (new_size > vec->size)                                               \
        vector_##NAME##_push_back(vec, kVectorPodDefaultValue_##NAME);         \
                                                                               \
    while (new_size < vec->size)                                               \
        vector_##NAME##_pop_back(vec);                                         \
}                                                                              \
                                                                               \
void vector_##NAME##_reserve(vector_##NAME##_t vec, size_t new_capacity)       \
{                                                                              \
    value_type* new_arr = NULL;                                                \
                                                                               \
    assert(vec != NULL);                                                       \
                                                                               \
    if (new_capacity <= vec->capacity) /* already have that reserved */        \
        return;                                                                \
                                                                               \
    new_arr = realloc(vec->arr, new_capacity*sizeof(value_type));              \
    assert(new_arr != NULL || new_capacity == 0);                              \
                                                                               \
    vec->arr = new_arr;                                                        \
    vec->capacity = new_capacity;                                              \
}                                                                              \
                                                                               \
void vector_##NAME##_erase(vector_##NAME##_t vec, size_t index)                \
{                                                                              \
    assert(vec != NULL);                                                       \
    assert(index < vector_##NAME##_size(vec));                                 \
                                                                               \
    if (index != vec->size - 1)                                                \
        memmove(vec->arr + index, vec->arr + index + 1,                        \
                (vec->size - index - 1)*sizeof(value_type));                   \
                                                                               \
    --vec->size;                                                               \
}                                                                              \
                                                                               \
void vector_##NAME##_clear(vector_##NAME##_t vec)                              \
{                                                                              \
    assert(vec != NULL);                                                       \
                                                                               \
    vec->size = 0;                                                             \
}                                                                              \
                                                                               \
void vector_##NAME##_fit(vector_##NAME##_t vec)                                \
{                                                                              \
    assert(vec != NULL);                                                       \
                                                                               \
    vector_##NAME##_shrink(vec);                                               \
}                                                                              \

/*** IMPLEMENTATION MACRO END ***/

/*** CODE END ***/

#endif
