/*
 * hashmap_pod_macro.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _HASHMAP_POD_MACRO_H_
#define _HASHMAP_POD_MACRO_H_

#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#include <vector/vector_pod_macro/vector_pod_macro.h>

/*** MACHINE-GENERATED ***/

/*** CODE BEGIN ***/

/*** DECLARATION MACRO BEGIN ***/

#define NOQUALIF

#define DECLARE_HASHMAP_POD(NAME, key_type, value_type)                        \
	__DECLARE_HASHMAP_POD(NAME, key_type, value_type, NOQUALIF)

#define DECLARE_STATIC_HASHMAP_POD(NAME, key_type, value_type)                 \
	__DECLARE_HASHMAP_POD(NAME, key_type, value_type, static)

#define __DECLARE_HASHMAP_POD(NAME, key_type, value_type, QUALIF)              \
                                                                               \
/**                                                                            \
 * This is a specialization of 'hashmap' intended to be used with POD elements. \
 * As such, contained element are not 'constructed' nor 'destroyed', and       \
 * copying is performed by simple byte mirroring.                              \
 * When needed, for instance when inserting new elements, a user-configured    \
 * 'default' element is used.                                                  \
 */                                                                            \
                                                                               \
/**                                                                            \
 * Hash operator for keys in the hashmap.                                      \
 */                                                                            \
typedef size_t (*hashmap_##NAME##_hash_f)(const key_type*);                    \
                                                                               \
/**                                                                            \
 * Equality operator for keys in the hashmap.                                  \
 */                                                                            \
typedef bool (*hashmap_##NAME##_key_eq_f)(const key_type* k1, const key_type* k2); \
                                                                               \
/**                                                                            \
 * Callback function used when iterating over the hashmap, where,              \
 *   k      The elements' key.                                                 \
 *   v      The elements' value.                                               \
 */                                                                            \
typedef void (*hashmap_##NAME##_foreach_f)(const key_type* k, value_type* v);  \
                                                                               \
/**                                                                            \
 * Callback function used when iterating over the hashmap, where,              \
 *   k      The elements' key.                                                 \
 *   v      The elements' value.                                               \
 *   d      Opaque object pointer reserved for the user's contextual data.     \
 */                                                                            \
typedef void (*hashmap_##NAME##_foreach_data_f)(const key_type* k, value_type* v, \
        void* d);                                                              \
                                                                               \
/**                                                                            \
 * Callback function used when iterating over the hashmap, where,              \
 *   k      The elements' key.                                                 \
 *   v      The elements' value.                                               \
 *   f      Opaque function pointer reserved for the user's contextual data.   \
 */                                                                            \
typedef void (*hashmap_##NAME##_foreach_func_f)(const key_type* k, value_type* v, \
        void(*f)());                                                           \
                                                                               \
typedef struct hashmap_##NAME* hashmap_##NAME##_t;                             \
                                                                               \
/**                                                                            \
 * Constructor.                                                                \
 * Construct a hashmap with hash operator 'hash' and initial size 'size'.      \
 * The 'size' parameter is constrained by other configuration paramenters (such \
 * as maximum load factor and grow factor), the value provided by the user will \
 * be automatically adjusted if necessary.                                     \
 * Note that this size currently refers to the table (the amount of 'buckets', \
 * in hash table parlance) and not to the actual amount of elements that can   \
 * be stored without reallocation (which is lower).                            \
 * This is a limitation of the current implementation.                         \
 *                                                                             \
 * Pre: hash != NULL                                                           \
 * Post: hashmap_##NAME##_create(hash, size) != NULL                           \
 */                                                                            \
QUALIF hashmap_##NAME##_t hashmap_##NAME##_create(hashmap_##NAME##_hash_f hash, size_t size); \
                                                                               \
/**                                                                            \
 * Destructor.                                                                 \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 */                                                                            \
QUALIF void hashmap_##NAME##_destroy(hashmap_##NAME##_t hm);                   \
                                                                               \
/**                                                                            \
 * Copy constructor.                                                           \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 * Post: hashmap_##NAME##_copy(hm) != NULL                                     \
 */                                                                            \
QUALIF hashmap_##NAME##_t hashmap_##NAME##_copy(hashmap_##NAME##_t hm);        \
                                                                               \
/**                                                                            \
 * Set the hashmap's maximum load factor.                                      \
 * The maximum load factor controls the average amount of collisions per       \
 * element in the hashmap. It is constrained by other paramenters (such as     \
 * current table size and grow factor), the value provided by the user will be \
 * automatically adjusted if necessary.                                        \
 *                                                                             \
 * Pre: hm != NULL && 0 < max_load                                             \
 */                                                                            \
QUALIF void hashmap_##NAME##_set_max_load(hashmap_##NAME##_t hm, float max_load); \
                                                                               \
/**                                                                            \
 * Return the hashmap's maximum load factor.                                   \
 * The maximum load factor controls the average amount of collisions per       \
 * element in the hashmap.                                                     \
 *                                                                             \
 * Pre: hm != NULL && 0 < hashmap_##NAME##_get_max_load(hm)                    \
 */                                                                            \
QUALIF float hashmap_##NAME##_get_max_load(hashmap_##NAME##_t hm);             \
                                                                               \
/**                                                                            \
 * Set the hashmap's grow factor.                                              \
 * When the hashmap needs to grow it does so by this factor. It is constrained \
 * by other paramenters (such as current table size and maximum load factor),  \
 * the value provided by the user will be automatically adjusted if necessary. \
 *                                                                             \
 * Pre: hm != NULL && 1 < grow_factor                                          \
 */                                                                            \
QUALIF void hashmap_##NAME##_set_grow_factor(hashmap_##NAME##_t hm, float grow_factor); \
                                                                               \
/**                                                                            \
 * Return the hashmap's grow factor.                                           \
 * When the hashmap needs to grow it does so by this factor.                   \
 *                                                                             \
QUALIF  * Pre: hm != NULL && 1 < hashmap_##NAME##_get_grow_factor(hm);         \
 */                                                                            \
QUALIF float hashmap_##NAME##_get_grow_factor(hashmap_##NAME##_t hm);          \
                                                                               \
/**                                                                            \
 * Return a pointer to the value associated with key 'key' in the hashmap, if  \
 * such key exists. Otherwise return NULL.                                     \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 */                                                                            \
QUALIF value_type* hashmap_##NAME##_find(hashmap_##NAME##_t hm, key_type key); \
                                                                               \
/**                                                                            \
 * Return a pointer to the value associated with key 'key' in the hashmap. If  \
 * such key did not exist, insert it associated with a default value and       \
 * return a pointer to this.                                                   \
 * Note that this operation can resize the hashmap, potentially invalidating   \
 * other pointers into the hashmap.                                            \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 * Post: hashmap_##NAME##_at(hm, key) != NULL                                  \
 */                                                                            \
QUALIF value_type* hashmap_##NAME##_at(hashmap_##NAME##_t hm, key_type key);   \
                                                                               \
/**                                                                            \
 * Change the hashmap's size.                                                  \
 * This function can only increase the size.                                   \
 * Note that this size currently refers to the table (the amount of 'buckets', \
 * in hash table parlance) and not to the actual amount of elements that can   \
 * be stored without reallocation (which is lower).                            \
 * This is a limitation of the current implementation.                         \
 * Note that this a potentially expensive operation.                           \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 * Post: hashmap_##NAME##_size(hm) >= new_size                                 \
 */                                                                            \
QUALIF void hashmap_##NAME##_resize(hashmap_##NAME##_t hm, size_t new_size);   \
                                                                               \
/**                                                                            \
 * Remove element with key 'key'.                                              \
 * Note: actual removal is deferred.                                           \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 */                                                                            \
QUALIF void hashmap_##NAME##_erase(hashmap_##NAME##_t hm, key_type key);       \
                                                                               \
/**                                                                            \
 * Remove all elements from the hashmap.                                       \
 * Note: actual removal is deferred.                                           \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 * Post: hashmap_##NAME##_size(hm) == 0                                        \
 */                                                                            \
QUALIF void hashmap_##NAME##_clear(hashmap_##NAME##_t hm);                     \
                                                                               \
/**                                                                            \
 * Actually remove erased elements from the hashmap.                           \
 * Note that this function will not waive memory back to the system.           \
 * If you plan to reuse the same hashmap with a significatively different key  \
 * domain, this operation can be useful to reduce "harmful collisions".        \
 * If instead you plan to reuse it with the same or a significatively similar  \
 * key domain, then you would probably be better off by leaving it as is.      \
 * This behaviour results from the fact that the current implementation does   \
 * not actually remove elements when they are "erased", instead it just marks  \
 * them as such and defers a potentially costly operation until a resize or    \
 * fit.                                                                        \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 */                                                                            \
QUALIF void hashmap_##NAME##_compact(hashmap_##NAME##_t hm);                   \
                                                                               \
/**                                                                            \
 * Return the amount of elements in the hashmap.                               \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 */                                                                            \
QUALIF size_t hashmap_##NAME##_size(hashmap_##NAME##_t hm);                    \
                                                                               \
/**                                                                            \
 * Return the amount of buckets in the hashmap.                                \
 * Debugging only.                                                             \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 */                                                                            \
QUALIF size_t hashmap_##NAME##_buckets(hashmap_##NAME##_t hm);                 \
                                                                               \
/**                                                                            \
 * Call 'f' for each element in the hashmap, in the order they were originally \
 * added.                                                                      \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 */                                                                            \
QUALIF void hashmap_##NAME##_foreach(hashmap_##NAME##_t hm, hashmap_##NAME##_foreach_f f); \
                                                                               \
/**                                                                            \
 * Call 'f' for each element in the hashmap, in the order they were originally \
 * added. Pass 'data' along to 'f'.                                            \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 */                                                                            \
void hashmap_##NAME##_foreach_data(hashmap_##NAME##_t hm, hashmap_##NAME##_foreach_data_f f, \
        void* data);                                                           \
                                                                               \
/**                                                                            \
 * Call 'f' for each element in the hashmap, in the order they were originally \
 * added. Pass 'func' along to 'f'.                                            \
 *                                                                             \
 * Pre: hm != NULL                                                             \
 */                                                                            \
void hashmap_##NAME##_foreach_func(hashmap_##NAME##_t hm, hashmap_##NAME##_foreach_func_f f, \
        void(*func)());                                                        \
                                                                               \
/**                                                                            \
 * Forward iterators. Quick and dirty.                                         \
 *                                                                             \
 * These iterators allow for sequential iteration over the elements in the     \
 * hashmap, in the order they were originally added.                           \
 *                                                                             \
 * An important feature: unlike pointer, these iterators are not invalidated   \
 * when the hashmap reallocates memory. Additionally an algorithm can add      \
 * elements to the hashmap while in the midst of an iteration, and have those  \
 * elements visited next (eventually) during the same iteration.               \
 *                                                                             \
 * An importat caveat wrt the current implementation: unlike pointers, these   \
 * iterators require the hashmap to be compact (not to have a non-erased       \
 * elements after an erased one, i.e. not to have holes).                      \
 */                                                                            \
                                                                               \
typedef size_t hashmap_##NAME##_iterator_t;                                    \
                                                                               \
/**                                                                            \
 * Return an iterator to the beginning.                                        \
 *                                                                             \
 * Pre: hm != NULL && hm doesn't have erased elements                          \
 */                                                                            \
QUALIF hashmap_##NAME##_iterator_t hashmap_##NAME##_iterator_begin(hashmap_##NAME##_t hm); \
                                                                               \
/**                                                                            \
 * Return true if 'it' is at the end of the iteration; otherwise false.        \
 *                                                                             \
 * Pre: hm != NULL && hm doesn't have erased elements                          \
 */                                                                            \
QUALIF bool hashmap_##NAME##_iterator_end(hashmap_##NAME##_t hm, hashmap_##NAME##_iterator_t it); \
                                                                               \
/**                                                                            \
 * Return an iterator to the next element. If the iterator is already at       \
 * the end, then return the same iterator.                                     \
 *                                                                             \
 * Pre: hm != NULL && hm doesn't have erased elements                          \
 */                                                                            \
hashmap_##NAME##_iterator_t hashmap_##NAME##_iterator_next(hashmap_##NAME##_t hm, \
        hashmap_##NAME##_iterator_t it);                                       \
                                                                               \
/**                                                                            \
 * Return a pointer to the value of the element associated with iterator 'it'. \
 *                                                                             \
 * Pre: hm != NULL && hm doesn't have erased elements &&                       \
 *      !hashmap_##NAME##_iterator_end(hm, it)                                 \
 * Post: hashmap_##NAME##_iterator_at(hm, it) != NULL                          \
 */                                                                            \
value_type* hashmap_##NAME##_iterator_at(hashmap_##NAME##_t hm,                \
        hashmap_##NAME##_iterator_t it);                                       \
                                                                               \
/**                                                                            \
 * Return a pointer to the key of the element associated with iterator 'it'.   \
 *                                                                             \
 * Pre: hm != NULL && hm doesn't have erased elements &&                       \
 *      !hashmap_##NAME##_iterator_end(hm, it)                                 \
 * Post: hashmap_##NAME##_iterator_key(hm, it) != NULL                         \
 */                                                                            \
const key_type* hashmap_##NAME##_iterator_key(hashmap_##NAME##_t hm,           \
        hashmap_##NAME##_iterator_t it);                                       \

/*** DECLARATION MACRO END ***/

/*** IMPLEMENTATION MACRO BEGIN ***/

#define KGRP(...) __VA_ARGS__

#define ENTRY_INITIALIZER(KEY, VALUE) { KEY, VALUE, false, NULL }

#define IMPLEMENT_HASHMAP_POD(NAME, key_type, value_type, DEFAULT_MAX_LOAD, DEFAULT_GROW_FACTOR, DEFAULT_KEY, DEFAULT_VALUE, KEY_EQ) \
                                                                               \
typedef struct entry_##NAME* entry_##NAME##_t;                                 \
                                                                               \
struct entry_##NAME {                                                          \
    key_type key;                                                              \
    value_type val;                                                            \
    bool erased;                                                               \
    entry_##NAME##_t next;                                                     \
};                                                                             \
                                                                               \
DECLARE_STATIC_VECTOR_POD(entry_##NAME, struct entry_##NAME)                   \
DECLARE_STATIC_VECTOR_POD(entryptr_##NAME, entry_##NAME##_t)                   \
                                                                               \
struct hashmap_##NAME {                                                        \
    vector_entryptr_##NAME##_t buckets;                                        \
    vector_entry_##NAME##_t store;                                             \
    float max_load;                                                            \
    float grow_factor;                                                         \
    size_t erased_count;                                                       \
    hashmap_##NAME##_hash_f hash;                                              \
    hashmap_##NAME##_key_eq_f key_eq;                                          \
};                                                                             \
                                                                               \
static const float kHashmapPodDefaultMaxLoad_##NAME = DEFAULT_MAX_LOAD;        \
static const float kHashmapPodDefaultGrowFactor_##NAME = DEFAULT_GROW_FACTOR;  \
static const key_type kHashmapPodDefaultKey_##NAME = DEFAULT_KEY;              \
static const value_type kHashmapPodDefaultValue_##NAME = DEFAULT_VALUE;        \
static const hashmap_##NAME##_key_eq_f kHashmapPodKeyEq_##NAME = KEY_EQ;       \
                                                                               \
IMPLEMENT_VECTOR_POD(entry_##NAME, struct entry_##NAME, VGP_ADHOC, 1.6,        \
       ENTRY_INITIALIZER(KGRP(DEFAULT_KEY), KGRP(DEFAULT_VALUE)))              \
IMPLEMENT_VECTOR_POD(entryptr_##NAME, entry_##NAME##_t, VGP_ADHOC, 1.6, NULL)  \
                                                                               \
static hashmap_##NAME##_t hashmap_##NAME##_create_common(size_t buckets_size,  \
        float max_load, float grow_factor, hashmap_##NAME##_hash_f hash,       \
        hashmap_##NAME##_key_eq_f key_eq);                                     \
static entry_##NAME##_t hashmap_##NAME##_lookup(hashmap_##NAME##_t hm, key_type key); \
static void hashmap_##NAME##_remove_and_rehash(hashmap_##NAME##_t hm);         \
                                                                               \
/*                                                                             \
 * Setup members: buckets, store, max_load, grow_factor, erased, hash, key_eq. \
 * Note that this function assumes that all arguments have already been        \
 * verified and are sane.                                                      \
 *                                                                             \
 * Post: hashmap_##NAME##_create_common(...) != NULL                           \
 */                                                                            \
static hashmap_##NAME##_t hashmap_##NAME##_create_common(size_t buckets_size,  \
        float max_load, float grow_factor, hashmap_##NAME##_hash_f hash,       \
        hashmap_##NAME##_key_eq_f key_eq)                                      \
{                                                                              \
    hashmap_##NAME##_t hm = NULL;                                              \
                                                                               \
    hm = malloc(sizeof(*hm));                                                  \
    assert(hm != NULL);                                                        \
                                                                               \
    hm->max_load = max_load;                                                   \
    hm->grow_factor = grow_factor;                                             \
    hm->erased_count = 0;                                                      \
    hm->hash = hash;                                                           \
    hm->key_eq = key_eq;                                                       \
                                                                               \
    hm->buckets = vector_entryptr_##NAME##_create(buckets_size);               \
                                                                               \
    vector_entryptr_##NAME##_resize(hm->buckets, buckets_size);                \
                                                                               \
    hm->store = vector_entry_##NAME##_create(buckets_size*hm->max_load);       \
                                                                               \
    return hm;                                                                 \
}                                                                              \
                                                                               \
hashmap_##NAME##_t hashmap_##NAME##_create(hashmap_##NAME##_hash_f hash, size_t size) \
{                                                                              \
    hashmap_##NAME##_t hm = NULL;                                              \
    float lboundf;                                                             \
                                                                               \
    assert(hash != NULL);                                                      \
    assert(0 < kHashmapPodDefaultMaxLoad_##NAME);                              \
    assert(1 < kHashmapPodDefaultGrowFactor_##NAME);                           \
    assert(kHashmapPodKeyEq_##NAME != NULL);                                   \
                                                                               \
    /*                                                                         \
     * in the following, note that the actual lower bound is ceil(lboundf) but, \
     * ceil(lboundf) <= floor(lboundf + 1)                                     \
     * and,                                                                    \
     * lboundf > 0 => trunc(lboundf + 1) = floor(lboundf + 1)                  \
     */                                                                        \
    lboundf = (1 + 1/kHashmapPodDefaultMaxLoad_##NAME) /                       \
                                (kHashmapPodDefaultGrowFactor_##NAME - 1);     \
    if (size < lboundf)                                                        \
        size = lboundf + 1;                                                    \
                                                                               \
    hm = hashmap_##NAME##_create_common(size, kHashmapPodDefaultMaxLoad_##NAME, \
            kHashmapPodDefaultGrowFactor_##NAME, hash, kHashmapPodKeyEq_##NAME); \
                                                                               \
    return hm;                                                                 \
}                                                                              \
                                                                               \
void hashmap_##NAME##_destroy(hashmap_##NAME##_t hm)                           \
{                                                                              \
    assert(hm != NULL);                                                        \
                                                                               \
    vector_entryptr_##NAME##_destroy(hm->buckets);                             \
    vector_entry_##NAME##_destroy(hm->store);                                  \
    free(hm);                                                                  \
}                                                                              \
                                                                               \
hashmap_##NAME##_t hashmap_##NAME##_copy(hashmap_##NAME##_t hm)                \
{                                                                              \
    hashmap_##NAME##_t copy = NULL;                                            \
    entry_##NAME##_t e = NULL;                                                 \
    size_t hm_store_size;                                                      \
    size_t i;                                                                  \
                                                                               \
    assert(hm != NULL);                                                        \
                                                                               \
    copy = hashmap_##NAME##_create_common(vector_entryptr_##NAME##_size(hm->buckets), \
            hm->max_load, hm->grow_factor, hm->hash, hm->key_eq);              \
                                                                               \
    hm_store_size = vector_entry_##NAME##_size(hm->store);                     \
                                                                               \
    for (i = 0; i < hm_store_size; ++i) {                                      \
        e = vector_entry_##NAME##_at(hm->store, i);                            \
        if (!e->erased)                                                        \
            vector_entry_##NAME##_push_back(copy->store, *e); /* assuming copyable pod */ \
    }                                                                          \
                                                                               \
    hashmap_##NAME##_remove_and_rehash(copy);                                  \
                                                                               \
    return copy;                                                               \
}                                                                              \
                                                                               \
void hashmap_##NAME##_set_max_load(hashmap_##NAME##_t hm, float max_load)      \
{                                                                              \
    float lbound;                                                              \
                                                                               \
    assert(hm != NULL);                                                        \
    assert(0 < max_load);                                                      \
                                                                               \
    lbound = 1/(vector_entryptr_##NAME##_size(hm->buckets)*(hm->grow_factor - 1) - 1); \
                                                                               \
    if (max_load < lbound)                                                     \
        hm->max_load = lbound;                                                 \
    else                                                                       \
        hm->max_load = max_load;                                               \
}                                                                              \
                                                                               \
float hashmap_##NAME##_get_max_load(hashmap_##NAME##_t hm)                     \
{                                                                              \
    assert(hm != NULL);                                                        \
                                                                               \
    return hm->max_load;                                                       \
}                                                                              \
                                                                               \
void hashmap_##NAME##_set_grow_factor(hashmap_##NAME##_t hm, float grow_factor) \
{                                                                              \
    float lbound;                                                              \
    size_t buckets_size;                                                       \
                                                                               \
    assert(hm != NULL);                                                        \
    assert(1 < grow_factor);                                                   \
                                                                               \
    buckets_size = vector_entryptr_##NAME##_size(hm->buckets);                 \
    lbound = 1 + 1/buckets_size + 1/(buckets_size*hm->max_load);               \
                                                                               \
    if (grow_factor < lbound)                                                  \
        hm->grow_factor = lbound;                                              \
    else                                                                       \
        hm->grow_factor = grow_factor;                                         \
}                                                                              \
                                                                               \
float hashmap_##NAME##_get_grow_factor(hashmap_##NAME##_t hm)                  \
{                                                                              \
    assert(hm != NULL);                                                        \
                                                                               \
    return hm->grow_factor;                                                    \
}                                                                              \
                                                                               \
/**                                                                            \
 * Lookup an entry with key 'key' in the hashmap and return a pointer to it    \
 * (even if it's marked as erased). If such entry does not exist return NULL.  \
 * Pre: hm != NULL                                                             \
 */                                                                            \
static entry_##NAME##_t hashmap_##NAME##_lookup(hashmap_##NAME##_t hm, key_type key) \
{                                                                              \
    entry_##NAME##_t e = NULL;                                                 \
    size_t buckets_size;                                                       \
    size_t ix;                                                                 \
                                                                               \
    assert(hm != NULL);                                                        \
                                                                               \
    buckets_size = vector_entryptr_##NAME##_size(hm->buckets);                 \
    ix = hm->hash(&key) % buckets_size;                                        \
                                                                               \
    /* see if key already exists */                                            \
    for (e = *vector_entryptr_##NAME##_at(hm->buckets, ix); e != NULL; e = e->next) \
        if (hm->key_eq(&e->key, &key))                                         \
            return e;                                                          \
                                                                               \
    /* key is not in the table */                                              \
                                                                               \
    return NULL;                                                               \
}                                                                              \
                                                                               \
value_type* hashmap_##NAME##_find(hashmap_##NAME##_t hm, key_type key)         \
{                                                                              \
    entry_##NAME##_t e = NULL;                                                 \
                                                                               \
    assert(hm != NULL);                                                        \
                                                                               \
    e = hashmap_##NAME##_lookup(hm, key);                                      \
    return e && !e->erased ? &e->val : NULL;                                   \
}                                                                              \
                                                                               \
value_type* hashmap_##NAME##_at(hashmap_##NAME##_t hm, key_type key)           \
{                                                                              \
    entry_##NAME##_t e = NULL;                                                 \
    entry_##NAME##_t* bucket_ptr = NULL;                                       \
    struct entry_##NAME new_entry;                                             \
    size_t buckets_size;                                                       \
    size_t store_size;                                                         \
    size_t ix;                                                                 \
                                                                               \
    assert(hm != NULL);                                                        \
                                                                               \
    buckets_size = vector_entryptr_##NAME##_size(hm->buckets);                 \
    store_size = vector_entry_##NAME##_size(hm->store);                        \
    ix = hm->hash(&key) % buckets_size;                                        \
                                                                               \
    /* see if key already exists */                                            \
    for (e = *vector_entryptr_##NAME##_at(hm->buckets, ix); e != NULL; e = e->next) { \
        if (hm->key_eq(&e->key, &key)) {                                       \
            if (e->erased) {                                                   \
                e->erased = false;                                             \
                e->val = kHashmapPodDefaultValue_##NAME;                       \
                --hm->erased_count;                                            \
            }                                                                  \
            return &e->val;                                                    \
        }                                                                      \
    }                                                                          \
                                                                               \
    /* key is not in the table */                                              \
                                                                               \
    /*                                                                         \
     * table too full? ie, need to resize?                                     \
     * note: this test could be replaced with,                                 \
     * vector_entry_##NAME##_size(hm->store) == vector_entry_##NAME##_capacity(hm->store) \
     */                                                                        \
    if (buckets_size*hm->max_load < store_size + 1) {                          \
        hashmap_##NAME##_resize(hm, buckets_size*hm->grow_factor);             \
        return hashmap_##NAME##_at(hm, key);                                   \
    }                                                                          \
                                                                               \
    /* add key to the start of the bucket */                                   \
    new_entry.key = key;                                                       \
    new_entry.val = kHashmapPodDefaultValue_##NAME;                            \
    new_entry.erased = false;                                                  \
    bucket_ptr = vector_entryptr_##NAME##_at(hm->buckets, ix);                 \
    new_entry.next = *bucket_ptr;                                              \
    e = vector_entry_##NAME##_push_back(hm->store, new_entry);                 \
    *bucket_ptr = e;                                                           \
                                                                               \
    return &e->val;                                                            \
}                                                                              \
                                                                               \
/**                                                                            \
 * Actually remove erased elements and rehash the whole hashmap.               \
 * Pre: hm != NULL                                                             \
 */                                                                            \
static void hashmap_##NAME##_remove_and_rehash(hashmap_##NAME##_t hm)          \
{                                                                              \
    entry_##NAME##_t elem = NULL;                                              \
    entry_##NAME##_t* bucket_ptr = NULL;                                       \
    size_t bucket_ix;                                                          \
    size_t buckets_size;                                                       \
    size_t store_size;                                                         \
    size_t i;                                                                  \
                                                                               \
    assert(hm != NULL);                                                        \
                                                                               \
    /*                                                                         \
     * fill the whole vector with empty buckets (NULL pointers)                \
     * note that we need to clear first so that resize touches all elements    \
     */                                                                        \
    vector_entryptr_##NAME##_clear(hm->buckets);                               \
    vector_entryptr_##NAME##_resize(hm->buckets, vector_entryptr_##NAME##_capacity(hm->buckets)); \
                                                                               \
    buckets_size = vector_entryptr_##NAME##_size(hm->buckets);                 \
    store_size = vector_entry_##NAME##_size(hm->store);                        \
                                                                               \
    /* actually remove marked entries */                                       \
    if (store_size == hm->erased_count) {                                      \
        vector_entry_##NAME##_clear(hm->store);                                \
        hm->erased_count = 0;                                                  \
        return;                                                                \
    }                                                                          \
    for (i = store_size - 1; /*0 <= i &&*/ hm->erased_count != 0; --i) {       \
        if (vector_entry_##NAME##_at(hm->store, i)->erased) {                  \
            vector_entry_##NAME##_erase(hm->store, i);                         \
            --hm->erased_count;                                                \
        }                                                                      \
    }                                                                          \
                                                                               \
    /* rehash */                                                               \
    store_size = vector_entry_##NAME##_size(hm->store);                        \
    for (i = 0; i < store_size; ++i) {                                         \
        elem = vector_entry_##NAME##_at(hm->store, i);                         \
        bucket_ix = hm->hash(&elem->key) % buckets_size;                       \
        bucket_ptr = vector_entryptr_##NAME##_at(hm->buckets, bucket_ix);      \
        /* link elements in the same bucket */                                 \
        elem->next = *bucket_ptr;                                              \
        *bucket_ptr = elem;                                                    \
    }                                                                          \
}                                                                              \
                                                                               \
void hashmap_##NAME##_resize(hashmap_##NAME##_t hm, size_t new_size)           \
{                                                                              \
    assert(hm != NULL);                                                        \
                                                                               \
    if (new_size <= vector_entryptr_##NAME##_size(hm->buckets))                \
        return;  /* already have that much reserved */                         \
                                                                               \
    /* reserve all the required memory */                                      \
    vector_entryptr_##NAME##_reserve(hm->buckets, new_size);                   \
    /* if needed, reallocate the store now */                                  \
    vector_entry_##NAME##_reserve(hm->store, hm->max_load*new_size);           \
                                                                               \
    hashmap_##NAME##_remove_and_rehash(hm);                                    \
}                                                                              \
                                                                               \
void hashmap_##NAME##_erase(hashmap_##NAME##_t hm, key_type key)               \
{                                                                              \
    entry_##NAME##_t e = NULL;                                                 \
                                                                               \
    assert(hm != NULL);                                                        \
                                                                               \
    e = hashmap_##NAME##_lookup(hm, key);                                      \
    if (e != NULL && !e->erased) {                                             \
        e->erased = true;                                                      \
        ++hm->erased_count;                                                    \
    }                                                                          \
}                                                                              \
                                                                               \
void hashmap_##NAME##_clear(hashmap_##NAME##_t hm)                             \
{                                                                              \
    entry_##NAME##_t e = NULL;                                                 \
    size_t store_size;                                                         \
    size_t i;                                                                  \
                                                                               \
    assert(hm != NULL);                                                        \
                                                                               \
    store_size = vector_entry_##NAME##_size(hm->store);                        \
                                                                               \
    for (i = 0; i < store_size; ++i) {                                         \
        e = vector_entry_##NAME##_at(hm->store, i);                            \
        if (!e->erased) {                                                      \
            e->erased = true;                                                  \
            ++hm->erased_count;                                                \
        }                                                                      \
    }                                                                          \
}                                                                              \
                                                                               \
void hashmap_##NAME##_compact(hashmap_##NAME##_t hm)                           \
{                                                                              \
    assert(hm != NULL);                                                        \
                                                                               \
    hashmap_##NAME##_remove_and_rehash(hm);                                    \
}                                                                              \
                                                                               \
size_t hashmap_##NAME##_size(hashmap_##NAME##_t hm)                            \
{                                                                              \
    assert(hm != NULL);                                                        \
                                                                               \
    return vector_entry_##NAME##_size(hm->store) - hm->erased_count;           \
}                                                                              \
                                                                               \
size_t hashmap_##NAME##_buckets(hashmap_##NAME##_t hm)                         \
{                                                                              \
    assert(hm != NULL);                                                        \
                                                                               \
    return vector_entryptr_##NAME##_size(hm->buckets);                         \
}                                                                              \
                                                                               \
void hashmap_##NAME##_foreach(hashmap_##NAME##_t hm, hashmap_##NAME##_foreach_f f) \
{                                                                              \
    entry_##NAME##_t e = NULL;                                                 \
    size_t i;                                                                  \
                                                                               \
    assert(hm != NULL);                                                        \
    assert(f != NULL);                                                         \
                                                                               \
    for (i = 0; i < vector_entry_##NAME##_size(hm->store); ++i) {              \
        e = vector_entry_##NAME##_at(hm->store, i);                            \
        if (!e->erased)                                                        \
            f(&e->key, &e->val);                                               \
    }                                                                          \
}                                                                              \
                                                                               \
void hashmap_##NAME##_foreach_data(hashmap_##NAME##_t hm, hashmap_##NAME##_foreach_data_f f, \
        void* data)                                                            \
{                                                                              \
    entry_##NAME##_t e = NULL;                                                 \
    size_t i;                                                                  \
                                                                               \
    assert(hm != NULL);                                                        \
    assert(f != NULL);                                                         \
                                                                               \
    for (i = 0; i < vector_entry_##NAME##_size(hm->store); ++i) {              \
        e = vector_entry_##NAME##_at(hm->store, i);                            \
        if (!e->erased)                                                        \
            f(&e->key, &e->val, data);                                         \
    }                                                                          \
}                                                                              \
                                                                               \
void hashmap_##NAME##_foreach_func(hashmap_##NAME##_t hm, hashmap_##NAME##_foreach_func_f f, \
        void(*func)())                                                         \
{                                                                              \
    entry_##NAME##_t e = NULL;                                                 \
    size_t i;                                                                  \
                                                                               \
    assert(hm != NULL);                                                        \
    assert(f != NULL);                                                         \
                                                                               \
    for (i = 0; i < vector_entry_##NAME##_size(hm->store); ++i) {              \
        e = vector_entry_##NAME##_at(hm->store, i);                            \
        if (!e->erased)                                                        \
            f(&e->key, &e->val, func);                                         \
    }                                                                          \
}                                                                              \
                                                                               \
/**                                                                            \
 * Return true if the hashmap is compact: in the store, the first non-erased   \
 * element is at position 0 and all non-erased elements are contiguous (i.e.   \
 * the store has no "holes"). Otherwise return false.                          \
 * Pre: hm != NULL                                                             \
 */                                                                            \
static bool hashmap_##NAME##_verify_for_iterator(hashmap_##NAME##_t hm)        \
{                                                                              \
    size_t i, existent = 0;                                                    \
                                                                               \
    assert(hm != NULL);                                                        \
                                                                               \
    for (i = 0; i < vector_entry_##NAME##_size(hm->store); ++i)                \
        if (vector_entry_##NAME##_at(hm->store, i)->erased)                    \
            break;                                                             \
        else                                                                   \
            ++existent;                                                        \
                                                                               \
    return existent == hashmap_##NAME##_size(hm);                              \
}                                                                              \
                                                                               \
hashmap_##NAME##_iterator_t hashmap_##NAME##_iterator_begin(hashmap_##NAME##_t hm) \
{                                                                              \
    assert(hm != NULL);                                                        \
    assert(hashmap_##NAME##_verify_for_iterator(hm));                          \
                                                                               \
    (void)hm;                                                                  \
                                                                               \
    return 0;                                                                  \
}                                                                              \
                                                                               \
bool hashmap_##NAME##_iterator_end(hashmap_##NAME##_t hm, hashmap_##NAME##_iterator_t it) \
{                                                                              \
    assert(hm != NULL);                                                        \
    assert(hashmap_##NAME##_verify_for_iterator(hm));                          \
                                                                               \
    return hashmap_##NAME##_size(hm) <= it;                                    \
}                                                                              \
                                                                               \
hashmap_##NAME##_iterator_t hashmap_##NAME##_iterator_next(hashmap_##NAME##_t hm, \
        hashmap_##NAME##_iterator_t it)                                        \
{                                                                              \
    assert(hm != NULL);                                                        \
    assert(hashmap_##NAME##_verify_for_iterator(hm));                          \
                                                                               \
    return hashmap_##NAME##_size(hm) <= it ? it : ++it;                        \
}                                                                              \
                                                                               \
value_type* hashmap_##NAME##_iterator_at(hashmap_##NAME##_t hm,                \
        hashmap_##NAME##_iterator_t it)                                        \
{                                                                              \
    assert(hm != NULL);                                                        \
    assert(hashmap_##NAME##_verify_for_iterator(hm));                          \
    assert(!hashmap_##NAME##_iterator_end(hm, it));                            \
                                                                               \
    return &vector_entry_##NAME##_at(hm->store, it)->val;                      \
}                                                                              \
                                                                               \
const key_type* hashmap_##NAME##_iterator_key(hashmap_##NAME##_t hm,           \
        hashmap_##NAME##_iterator_t it)                                        \
{                                                                              \
    assert(hm != NULL);                                                        \
    assert(hashmap_##NAME##_verify_for_iterator(hm));                          \
    assert(!hashmap_##NAME##_iterator_end(hm, it));                            \
                                                                               \
    return &vector_entry_##NAME##_at(hm->store, it)->key;                      \
}                                                                              \

/*** IMPLEMENTATION MACRO END ***/

/*** CODE END ***/

#endif
