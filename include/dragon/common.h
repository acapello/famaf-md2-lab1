#ifndef _DRAGON_COMMON_H_
#define _DRAGON_COMMON_H_

#if __GNUC__
#  if __x86_64__ || __ppc64__
#    define ENVIRONMENT64
#  else
#    define ENVIRONMENT32
#  endif
#endif

#include <stdlib.h>

#include <dragon/types.h>

/* Default initial allocation of vertices. */
#define COMMON_VERTICES_ALLOC  512

#define COMMON_MAX_LOAD        0.3
#define COMMON_GROW_FACTOR     1.618033989  /* golden */

/**
 * Equality operator for u32.
 * Used in hashmaps were the key is of type u32.
 */
bool u32_eq(const u32* a, const u32* b);

/**
 * Hash operator for u32.
 * Used in hashmaps were the key is of type u32.
 */
size_t u32_hash(const u32* key);

#endif
