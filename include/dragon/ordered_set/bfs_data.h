#ifndef _BFS_DATA_H_
#define _BFS_DATA_H_

#include <dragon/common.h>

struct bfs_data {
    u32 vertex;
    u32 parent;
    u32 epsilon;
    bool direction;  /* true => forward; false => backward */
};

#endif

