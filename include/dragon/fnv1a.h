#ifndef _FNV1A_H_
#define _FNV1A_H_

#include <stdlib.h>

#include <dragon/common.h>

/**
 * Fowler–Noll–Vo
 * Variant FNV-1a
 * http://www.isthe.com/chongo/tech/comp/fnv/index.html
 */

#define FNV1a_64_INITIAL 0xCBF29CE484222325
#define FNV1a_64_PRIME 0x100000001B3
#define FNV1a_32_INITIAL 0x811C9DC5
#define FNV1a_32_PRIME 0x01000193

#ifdef ENVIRONMENT64
#  define FNV1a_INITIAL FNV1a_64_INITIAL
#  define FNV1a_PRIME   FNV1a_64_PRIME
#else
#  define FNV1a_INITIAL FNV1a_32_INITIAL
#  define FNV1a_PRIME   FNV1a_32_PRIME
#endif  /* ENVIRONMENT64 */

#define FNV1a(data, size) FNV1a_state(data, size, FNV1a_INITIAL)

size_t FNV1a_state(const char* data, size_t size, size_t state);

#endif
