/*
 * parser.h
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef _PARSER_H_
#define _PARSER_H_

#include <stdio.h>

#include <dragon/common.h>

typedef struct parser* parser_t;

/**
 * Constructor.
 *
 * Pre: intput != NULL
 * Post: parser_create(input) != NULL
 */
parser_t parser_create(FILE* input);

/**
 * Destructor.
 *
 * Pre: p != NULL
 */
void parser_destroy(parser_t p);

/**
 * Return true if all input was exhausted.
 *
 * Pre: p != NULL
 */
bool parser_finished(parser_t p);

/**
 * Attempt to parse an edge of the form,
 *    <u><s><u><s><u><e>
 * where:
 *    <u> represents an unsigned decimal integer that fits into an unsigned
 * long integer type;
 *    <s> represents a single space character; and
 *    <e> represents either a LF character or EOF.
 * If successful put the parsed integers into *x, *y and *c successively and
 * return true.
 *
 * Pre: p != NULL && !parser_finished(p)
 */
bool parser_parse_edge(parser_t p, u32* x, u32* y, u32* c);

#endif
