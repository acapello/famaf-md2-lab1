#ifndef _DRAGON_H_
#define _DRAGON_H_

#include <dragon/common.h>

typedef struct DragonSt* DragonP;

/**
 * Returns a pointer to a newly created, empty DragonSt.
 *
 * The caller must call DestruirDragon when done using the resulting
 * DragonSt, so the resources allocated by this call are freed.
 */
DragonP NuevoDragon(void);

/**
 * Free the resources allocated for the given 'd'.
 * Returns 1 if it works normally, else returns 0.
 *
 * PRE: 'd' must not be NULL.
 */
int DestruirDragon(DragonP d);

/**
 * Add the edge (x,y) with capacity c in d.
 * Initialize the flow in this edge as 0.
 * Returns 1 if it works normally, else returns 0.
 *
 * PRE: 'd' must not be NULL.
 */
int CargarUnLado(DragonP d, u32 x, u32 y, u32 cap);

/**
 * Read the network from the stdin, and add the required data to d, using
 * CargarUnLado.
 * Returns 1 if it works normally and the network is valid, i.e.
 * ListoParaVolar(d) is true. Otherwise returns 0.
 *
 * PRE: 'd' must not be NULL.
 */
int LlenarDragon(DragonP d);

/**
 * Try to find an augmenting path of shortest length.
 * Returns 1 if it works normally, else returns 0.
 *
 * PRE: 'd' must not be NULL && ListoParaVolar(d)
 */
int ECAML(DragonP d);

/**
 * Return true if the drake is ready to take off, i.e, the network is valid,
 * has at least source and sink vertices.
 *
 * PRE: 'd' != NULL
 */
bool ListoParaVolar(DragonP d);

/**
 * Returns 100a+10b+c where:
 * a = 1 if the flow in d is a maximum, else 0.
 * b = 1 if the last ECAML arrived to t, else 0.
 * c = 1 if the results of the last ECAML updated the flow, else 0.
 *
 * PRE: 'd' must not be NULL.
 */
int DondeEstamosParados(DragonP d);

/*
 * Update the flow in d.
 * Returns the increment in the flow, else if there was any problem, returns
 * 0.
 *
 * PRE: 'd' must not be NULL. ECAML arrived to t, but the flow wan't yet
 *      updated && ListoParaVolar(d).
 */
u32 AumentarFlujo(DragonP d);

/**
 * Similar to AumentarFlujo, but also print the augmenting path to standard
 * output.
 * Returns the increment in the flow, else if there was any problem, returns
 * 0.
 *
 * PRE: 'd' must not be NULL. ECAML arrived to t, but the flow wan't yet
 *      updated && ListoParaVolar(d).
 */
u32 AumentarFlujoYtambienImprimirCamino(DragonP d);

/**
 * Print the updated flow to stdout.
 *
 * PRE: 'd' must not be NULL.
 */
void ImprimirFlujo(DragonP d);

/**
 * Print the updated flow value to stdout.
 *
 * PRE: 'd' must not be NULL.
 */
void ImprimirValorFlujo(DragonP d);

/**
 * Print the min cut of the network and its capacity to the stdout.
 *
 * PRE: 'd' must not be NULL && the current flow is maximal
 */
void ImprimirCorte(DragonP d);

/**
 * Add together a and b, return the result.
 */
u64 Sumar64(u64 a, u32 b);

#endif
