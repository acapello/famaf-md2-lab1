#ifndef _HASHMAP_EDGE_H_
#define _HASHMAP_EDGE_H_

#include <hashmap/hashmap_pod_macro/hashmap_pod_macro.h>
#include <dragon/common.h>
#include <dragon/graph/edge.h>

DECLARE_HASHMAP_POD(edge, u32, edge_t)

#endif
