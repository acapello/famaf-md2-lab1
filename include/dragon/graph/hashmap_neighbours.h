#ifndef _HASHMAP_NEIGHBOURS_H_
#define _HASHMAP_NEIGHBOURS_H_

#include <hashmap/hashmap_pod_macro/hashmap_pod_macro.h>
#include <dragon/common.h>
#include <dragon/graph/hashmap_edge.h>

typedef struct neighbours* neighbours_t;

struct neighbours {
    hashmap_edge_t forward;
    hashmap_edge_t backward;
};

DECLARE_HASHMAP_POD(neighbours, u32, struct neighbours)

#endif
