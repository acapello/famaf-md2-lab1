#ifndef _EDGE_H_
#define _EDGE_H_

#include <dragon/common.h>

typedef struct edge* edge_t;

struct edge {
    u32 capacity;
    u32 flow;
};

/**
 * Constructor.
 * Construct an edge with capacity 'capacity' and flow 0.
 *
 * Post: edge_create(capacity) != NULL
 */
edge_t edge_create(u32 capacity);

/**
 * Destructor.
 *
 * Pre: e != NULL
 */
void edge_destroy(edge_t e);

#endif
