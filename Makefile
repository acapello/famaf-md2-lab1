include config.make

LIBNAME := libdragon

OUTPUT_OBJ := $(CFG_BUILD_OUTPUT)/obj
OUTPUT_BIN := $(CFG_BUILD_OUTPUT)/bin
OUTPUT_LIB := $(CFG_BUILD_OUTPUT)/lib

TARGET_STATIC := $(OUTPUT_LIB)/$(LIBNAME).a
TARGET_SHARED := $(OUTPUT_LIB)/$(LIBNAME).so

INCLUDE := $(CFG_PROJECT_ROOT)/include

SOURCE_DIRS  := $(shell find src -type d)
SOURCE_FILES := $(shell find $(SOURCE_DIRS) -maxdepth 1 -mindepth 1 -name "*.c")

OBJECT_FILES  := $(SOURCE_FILES:.c=.o)
OBJECT_OUTPUT := $(addprefix $(OUTPUT_OBJ)/, $(OBJECT_FILES))

.PHONY : clean

# Replaced C90 for C99
#CFLAGS += -Wall -Wextra -Werror -pedantic -std=c90 -I$(INCLUDE) \
#                -Wno-unused-function -Wno-variadic-macros
CFLAGS += -Wall -Wextra -Werror -pedantic -std=c99 -I$(INCLUDE) \
		  -Wno-unused-function

all: $(CFG_DEFAULT_TARGET)

static-debug: CFLAGS += -g -O0
static-release: CFLAGS += -DNDEBUG -Ofast

static-debug: $(TARGET_STATIC)
static-release: $(TARGET_STATIC)

$(TARGET_STATIC): $(OBJECT_OUTPUT)
	@mkdir -p $(@D)
	ar rscv $@ $(OBJECT_OUTPUT)

$(OUTPUT_OBJ)/%.o: %.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) $(LDFLAGS) $(LIBS) -o $@ -c $<

clean:
	rm -rf $(OUTPUT_OBJ) $(OUTPUT_BIN) $(OUTPUT_LIB)
