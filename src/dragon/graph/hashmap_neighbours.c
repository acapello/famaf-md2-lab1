#include <dragon/graph/hashmap_neighbours.h>

#define DEFAULT_NEIGHBOURS {NULL, NULL}

IMPLEMENT_HASHMAP_POD(neighbours, u32, struct neighbours, COMMON_MAX_LOAD,
        COMMON_GROW_FACTOR, 0, DEFAULT_NEIGHBOURS, u32_eq)
