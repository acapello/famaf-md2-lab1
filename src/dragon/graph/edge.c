#include <assert.h>

#include <dragon/graph/edge.h>

edge_t edge_create(u32 capacity)
{
    edge_t e = malloc(sizeof(*e));
    assert(e != NULL);

    e->capacity = capacity;
    e->flow = 0;

    return e;
}

void edge_destroy(edge_t e)
{
    assert(e != NULL);

    free(e);
}
