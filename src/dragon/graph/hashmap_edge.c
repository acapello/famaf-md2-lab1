#include <dragon/graph/hashmap_edge.h>

IMPLEMENT_HASHMAP_POD(edge, u32, edge_t, COMMON_MAX_LOAD,
        COMMON_GROW_FACTOR, 0, NULL, u32_eq)
