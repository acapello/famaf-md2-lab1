#include <dragon/ordered_set/hashmap_bfs.h>

#define DEFAULT_BFS_DATA {-1, -1, -1, true}

IMPLEMENT_HASHMAP_POD(bfs, u32, struct bfs_data, COMMON_MAX_LOAD,
        COMMON_GROW_FACTOR, 0, DEFAULT_BFS_DATA, u32_eq)
