/*
 * ordered_set.c
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 * Copyright (c) 2013, Agustin Capello <aac0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#include <stdlib.h>
#include <assert.h>

#include <hashmap/hashmap_pod_macro/hashmap_pod_macro.h>
#include <dragon/ordered_set/ordered_set.h>
#include <dragon/ordered_set/hashmap_bfs.h>

struct ordered_set {
    hashmap_bfs_t hm;
    hashmap_bfs_iterator_t it;
};

ordered_set_t ordered_set_create(size_t size)
{
    ordered_set_t os = malloc(sizeof(*os));
    assert(os != NULL);

    os->hm = hashmap_bfs_create(u32_hash, size);

    ordered_set_rewind(os);

    return os;
}

void ordered_set_destroy(ordered_set_t os)
{
    assert(os != NULL);

    hashmap_bfs_destroy(os->hm);
    free(os);
}

set_data_t ordered_set_append(ordered_set_t os, set_elem_t el)
{
    assert(os != NULL);

    return hashmap_bfs_at(os->hm, el);
}

size_t ordered_set_size(ordered_set_t os)
{
    assert(os != NULL);

    return hashmap_bfs_size(os->hm);
}

void ordered_set_clear(ordered_set_t os)
{
    assert(os != NULL);

    hashmap_bfs_clear(os->hm);
    hashmap_bfs_compact(os->hm);  /* fuck */
    ordered_set_rewind(os);
}

set_data_t ordered_set_find(ordered_set_t os, set_elem_t el)
{
    assert(os != NULL);

    return hashmap_bfs_find(os->hm, el);
}

void ordered_set_rewind(ordered_set_t os)
{
    assert(os != NULL);

    os->it = hashmap_bfs_iterator_begin(os->hm);
}

set_data_t ordered_set_curr(ordered_set_t os)
{
    assert(os != NULL);

    if (hashmap_bfs_iterator_end(os->hm, os->it))
        return NULL;

    return hashmap_bfs_iterator_at(os->hm, os->it);
}

set_data_t ordered_set_next(ordered_set_t os)
{
    set_data_t data = NULL;

    assert(os != NULL);

    if (hashmap_bfs_iterator_end(os->hm, os->it))
        return NULL;

    data = hashmap_bfs_iterator_at(os->hm, os->it);
    os->it = hashmap_bfs_iterator_next(os->hm, os->it);
    return data;
}

#include <stdio.h>
static void print_elem(const u32* v, set_data_t data)
{
    printf("\t%u => (%u %u %u %u)\n", *v, data->vertex, data->parent,
            data->epsilon, data->direction);
}

void ordered_set_dump(ordered_set_t os)
{
    assert(os);

    printf("{ \n");
    hashmap_bfs_foreach(os->hm, print_elem);
    printf("}\n");
}
