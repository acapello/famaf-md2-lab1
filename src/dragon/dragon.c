/*
 * dragon.c
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 * Copyright (c) 2013, Agustin Capello <aac0111@famaf.unc.edu.ar>
 * Copyright (c) 2013, Ariel Wolfmann <amw0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#include <stdio.h>
#include <assert.h>

#include <dragon/dragon.h>
#include <dragon/parser.h>
#include <dragon/graph/graph.h>
#include <dragon/ordered_set/ordered_set.h>

#define SOURCE_VERTEX 0
#define SINK_VERTEX 1

#define MIN(a,b) ((b)<(a)?(b):(a))

static const char kDragonMaximal[] = "Maximal";
static const char kDragonNonMaximal[] = "(no maximal)";

struct DragonSt {
    graph_t network;
    parser_t parser;
    ordered_set_t set;
    u64 flow_value;
    bool flow_maximal;
    bool aug_path_found;
    bool aug_path_pending;
    unsigned aug_path_count;
};

DragonP NuevoDragon(void)
{
    DragonP d = calloc(1, sizeof(*d));
    if (d == NULL) {
        return NULL;
    }

    d->network = graph_create();
    d->parser = parser_create(stdin);
    d->set = ordered_set_create(COMMON_VERTICES_ALLOC);
    d->flow_value = 0;
    d->flow_maximal = false;
    d->aug_path_found = false;
    d->aug_path_pending = false;
    d->aug_path_count = 0;

    return d;
}

int DestruirDragon(DragonP d)
{
    assert(d != NULL);

    graph_destroy(d->network);
    parser_destroy(d->parser);
    ordered_set_destroy(d->set);
    free(d);

    return 1;
}

int CargarUnLado(DragonP d, u32 x, u32 y, u32 cap)
{
    assert(d != NULL);

    graph_new_directed_edge(d->network, x, y, cap);

    return 1;
}

int LlenarDragon(DragonP d)
{
    u32 x, y, c;

    assert(d != NULL);

    while (!parser_finished(d->parser) &&
            parser_parse_edge(d->parser, &x, &y, &c)) {
        CargarUnLado(d, x, y, c);
    }

    /*printf("n=%zu m=%zu\n", graph_vertices(d->network), graph_edges(d->network));*/

    return ListoParaVolar(d) ? 1 : 0;
}

/**
 * Callback for processing forward edges.
 */
static bool ECAML_forward_helper(u32 parent, u32 z, edge_t edge, void* data)
{
    DragonP d = (DragonP) data;
    set_data_t bfsd_parent = NULL;
    set_data_t bfsd = NULL;
    bool res = false;
    (void)parent;

    assert(d != NULL);
    assert(edge != NULL);

    /* if the vertex hasn't been seen already and has available capacity
       update the info in the set */
    if (!ordered_set_find(d->set, z) && edge->flow < edge->capacity) {
        bfsd = ordered_set_append(d->set, z);
        /* note that geting this pointer _after_ the append is important
           because the set could have been resized */
        bfsd_parent = ordered_set_curr(d->set);  /* current item */
        bfsd->vertex = z;
        bfsd->parent = bfsd_parent->vertex;
        bfsd->epsilon = MIN(bfsd_parent->epsilon, edge->capacity - edge->flow);
        bfsd->direction = true; /* forward */

        /* make the iteration stop when at the SINK */
        res = z == SINK_VERTEX;
    }

    return res;
}

/**
 * Callback for processing backward edges.
 */
static bool ECAML_backward_helper(u32 parent, u32 z, edge_t edge, void* data)
{
    DragonP d = (DragonP) data;
    set_data_t bfsd_parent = NULL;
    set_data_t bfsd = NULL;
    bool res = false;
    (void)parent;

    assert(d != NULL);
    assert(edge != NULL);

    /* if the vertex hasn't been seen already and has available capacity
       update the info in the set */
    if (!ordered_set_find(d->set, z) && edge->flow > 0) {
        bfsd = ordered_set_append(d->set, z);
        /* note that geting this pointer _after_ the append is important
           because the set could have been resized */
        bfsd_parent = ordered_set_curr(d->set);  /* current item */
        bfsd->vertex = z;
        bfsd->parent = bfsd_parent->vertex;
        bfsd->epsilon = MIN(bfsd_parent->epsilon, edge->flow);
        bfsd->direction = false; /* backward */

        /* make the iteration stop when at the SINK */
        res = z == SINK_VERTEX;
    }

    return res;
}

int ECAML(DragonP d)
{
    set_data_t bfsd = NULL;
    u32 bfsd_vertex;

    assert(d != NULL);
    assert(ListoParaVolar(d));

    ordered_set_clear(d->set);

    d->aug_path_pending = false;
    d->aug_path_found = false;

    bfsd = ordered_set_append(d->set, SOURCE_VERTEX);
    bfsd->vertex = SOURCE_VERTEX;

    while (!d->aug_path_found && (bfsd = ordered_set_curr(d->set))) {
        /* stash the value, the pointer could be invalidated because the
           helper functions will add items to the set */
        bfsd_vertex = bfsd->vertex;

        /* if we reached the sink then stop */
        if (graph_foreach_edge_forward(d->network, bfsd_vertex,
                    ECAML_forward_helper, d) ||
                graph_foreach_edge_backward(d->network, bfsd_vertex,
                    ECAML_backward_helper, d)) {
            d->aug_path_found = true;
            d->aug_path_pending = true;
            break;
        }
        ordered_set_next(d->set);
    }

    if (!d->aug_path_found) { /* didn't reach the sink */
        d->flow_maximal = true;
        return 0;
    }

    return 1;
}

bool ListoParaVolar(DragonP d)
{
    assert(d != NULL);

    return graph_vertex_exists(d->network, SOURCE_VERTEX) &&
        graph_vertex_exists(d->network, SINK_VERTEX);
}

int DondeEstamosParados(DragonP d)
{
    assert(d != NULL);

    return 100*(d->flow_maximal) + 10*(d->aug_path_found) +
        !d->aug_path_pending;
}

u32 AumentarFlujo(DragonP d)
{
    set_data_t bfsd = NULL;
    edge_t edge = NULL;
    u32 path_flow;

    assert(d != NULL);
    assert(ListoParaVolar(d));

    if (!d->aug_path_found || !d->aug_path_pending)
        return 0;

    bfsd = ordered_set_find(d->set, SINK_VERTEX);
    assert(bfsd != NULL);

    path_flow = bfsd->epsilon;

    while (bfsd->parent != SOURCE_VERTEX) {
        if (bfsd->direction) {  /* forward */
            edge = graph_directed_edge(d->network, bfsd->parent, bfsd->vertex);
            assert(edge != NULL);
            edge->flow += path_flow;
        } else {  /* backward */
            edge = graph_directed_edge(d->network, bfsd->vertex, bfsd->parent);
            assert(edge != NULL);
            edge->flow -= path_flow;
        }

        bfsd = ordered_set_find(d->set, bfsd->parent);
        assert(bfsd != NULL);
    }
    /* the path will have to start with a forward edge from the source */
    edge = graph_directed_edge(d->network, bfsd->parent, bfsd->vertex);
    assert(edge != NULL);
    edge->flow += path_flow;

    d->flow_value = Sumar64(d->flow_value, path_flow);
    ++d->aug_path_count;
    d->aug_path_pending = false;

    return path_flow;
}

u32 AumentarFlujoYtambienImprimirCamino(DragonP d)
{
    set_data_t bfsd = NULL;
    edge_t edge = NULL;
    u32 path_flow;

    assert(d != NULL);
    assert(ListoParaVolar(d));

    if (!d->aug_path_found || !d->aug_path_pending)
        return 0;

    printf("camino aumentante %u:\n", d->aug_path_count + 1);

    bfsd = ordered_set_find(d->set, SINK_VERTEX);
    assert(bfsd != NULL);

    path_flow = bfsd->epsilon;
    printf("t");

    while (bfsd->parent != SOURCE_VERTEX) {
        if (bfsd->direction) {  /* forward */
            edge = graph_directed_edge(d->network, bfsd->parent, bfsd->vertex);
            assert(edge != NULL);
            edge->flow += path_flow;
            printf(":");
        } else {  /* backward */
            edge = graph_directed_edge(d->network, bfsd->vertex, bfsd->parent);
            assert(edge != NULL);
            edge->flow -= path_flow;
            printf("|");
        }

        printf("%u", bfsd->parent);

        bfsd = ordered_set_find(d->set, bfsd->parent);
        assert(bfsd != NULL);
    }
    /* the path will have to start with a forward edge from the source */
    edge = graph_directed_edge(d->network, bfsd->parent, bfsd->vertex);
    assert(edge != NULL);
    edge->flow += path_flow;
    printf(":s:\t%u\n", path_flow);

    d->flow_value = Sumar64(d->flow_value, path_flow);
    ++d->aug_path_count;
    d->aug_path_pending = false;

    return path_flow;
}

/**
 * Callback used to print the flow in each edge.
 */
static bool ImprimirFlujo_helper(u32 x, u32 y, const edge_t e, void* data)
{
    assert(e != NULL);
    (void)data;

    printf("Lado %u,%u:\t%u\n", x, y, e->flow);
    return false;
}

void ImprimirFlujo(DragonP d)
{
    const char* ft = NULL;

    assert(d != NULL);

    if (d->flow_maximal)
        ft = kDragonMaximal;
    else
        ft = kDragonNonMaximal;

    printf("Flujo %s:\n", ft);

    graph_foreach_edge(d->network, ImprimirFlujo_helper, NULL);
}

/**
 * If the type is abstracted, so must be all operations on it.
 */
static void Imprimir64(const u64 v)
{
#if defined(ENVIRONMENT64)
    printf("%lu", v);
#elif defined(ENVIRONMENT32)
    printf("%llu", v);
#else
#  error "Could not determine the target architecture's word size."
#endif
}

void ImprimirValorFlujo(DragonP d)
{
    const char* ft = NULL;

    assert(d != NULL);

    if (d->flow_maximal)
        ft = kDragonMaximal;
    else
        ft = kDragonNonMaximal;

    printf("Valor del flujo %s:\t", ft);
    Imprimir64(d->flow_value);
    printf("\n");
}

/**
 * Folding operator used to sum the capacity of edges leading outside the cut.
 *  x           Edge's source vertex.
 *  z           Edge's destination vertex.
 *  edge        Edge's associated data.
 *  base        Folding partial result.
 *  data        Dragon object.
 */
static u64 ImprimirCorte_helper(u32 x, u32 z, edge_t edge, u64 base,
        void* data)
{
    DragonP d = (DragonP) data;

    assert(d != NULL);
    assert(edge != NULL);
    assert(ordered_set_find(d->set, x) != NULL); /* x is inside the cut */
    (void)x;

    /* if z is outside the cut, add the edge's capacity */
    if (!ordered_set_find(d->set, z))
        base = Sumar64(base, edge->capacity);

    return base;
}

void ImprimirCorte(DragonP d)
{
    set_data_t bfsd = NULL;
    u64 cut_capacity = 0;

    assert(d != NULL);
    assert(d->flow_maximal);

    ordered_set_rewind(d->set);

    printf("Corte Minimal:\tS = {");

    /* ugly kluge: assuming the source was the first vertex added to the set */
    bfsd = ordered_set_next(d->set);
    assert(bfsd->vertex == SOURCE_VERTEX);
    printf("s");
    cut_capacity = graph_foldl_edge_forward(d->network, bfsd->vertex,
            ImprimirCorte_helper, cut_capacity, d);

    while ((bfsd = ordered_set_next(d->set))) {
        printf(", %u", bfsd->vertex);
        cut_capacity = graph_foldl_edge_forward(d->network, bfsd->vertex,
                ImprimirCorte_helper, cut_capacity, d);
    }

    printf("}\n");

    printf("Capacidad:\t");
    Imprimir64(cut_capacity);
    printf("\n");
}

u64 Sumar64(u64 a, u32 b)
{
    return a + b;
}
