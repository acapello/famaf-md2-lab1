#include <dragon/fnv1a.h>

size_t FNV1a_state(const char* data, size_t size, size_t state)
{
    for (; size; --size) {
        state ^= *data++;
        state *= FNV1a_PRIME;
    }
    return state;
}
