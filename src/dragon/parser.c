/*
 * parser.c
 *
 * Copyright (c) 2013, Sebastian Portillo <msp0111@famaf.unc.edu.ar>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

#include <stdlib.h>
#include <assert.h>
#include <errno.h>

#include <bstring/bstrlib.h>
#include <lexer/lexer.h>
#include <dragon/parser.h>

#define WHITESPACES " "
#define LINE_FEED "\n"

struct parser {
    Lexer* lexer;
};

/**
 * Like strtoul.
 * If the conversion was successful return true and copy the converted
 * integer into *u.
 */
static bool bstrtou32(bstring bstr, u32* u)
{
    const char* str = NULL;
    char* endptr = NULL;
    unsigned long tmp = 0;

    assert(bstr != NULL);
    assert(u != NULL);

    str = bdata(bstr);

    errno = 0;
    tmp = strtoul(str, &endptr, 10);
    if (errno != 0 || *endptr != '\0' || tmp > UINT32_MAX)
        return false;

    *u = tmp;
    return true;
}

/*
 * If nothing was read by the lexer in the last operation, frees the memory
 * returned by lexer_item() and returns false.
 * If the lexer did read something, returns true. The memory returned by
 * lexer_item() is not freed and becomes the caller's responsability.
 */
static bool parser_lexer_did_read(parser_t p)
{
	assert(p != NULL);

    if (lexer_is_off(p->lexer))
        return false;

    if (blength(lexer_item(p->lexer)) == 0) {
        bdestroy(lexer_item(p->lexer));
        return false;
    }

    return true;
}

/**
 * Attempt to parse an unsigned integer long.
 * If successful put the parsed integer into *u and return true.
 */
static bool parser_parse_u32(parser_t p, u32* u)
{
    bstring str = NULL;

    assert(p != NULL);
    assert(u != NULL);

    if (lexer_is_off(p->lexer))
        return false;

    lexer_next(p->lexer, DIGIT);
    if (!parser_lexer_did_read(p))
        return false;

    str = lexer_item(p->lexer);

    if (!bstrtou32(str, u)) {
        bdestroy(str);
        return false;
    }
    bdestroy(str);

    return true;
}

/**
 * Attempt to parse a single whitespace.
 * Return true if successful.
 */
static bool parser_parse_ws(parser_t p)
{
    assert(p != NULL);

    if (lexer_is_off(p->lexer))
        return false;

    lexer_next_char(p->lexer, WHITESPACES);
	if (!parser_lexer_did_read(p))
        return false;
    bdestroy(lexer_item(p->lexer));

    return true;
}

/**
 * Attempt to parse either a LF (\n) or EOF.
 * Return true if successful.
 */
static bool parser_parse_end(parser_t p)
{
    assert(p != NULL);

    if (lexer_is_off(p->lexer))
        return true;

    lexer_next_char(p->lexer, LINE_FEED);
	if (!parser_lexer_did_read(p))
        return false;
    bdestroy(lexer_item(p->lexer));

    return true;
}

parser_t parser_create(FILE* input)
{
    parser_t p = NULL;

    assert(input != NULL);

    p = malloc(sizeof(*p));
    assert(p != NULL);

    p->lexer = lexer_new(input);

    return p;
}

void parser_destroy(parser_t p)
{
    assert(p != NULL);

    lexer_destroy(p->lexer);
    free(p);
}

bool parser_finished(parser_t p)
{
    assert(p != NULL);

    return lexer_is_off(p->lexer);
}

bool parser_parse_edge(parser_t p, u32* x, u32* y, u32* c)
{
    assert(p != NULL);
    assert(x != NULL);
    assert(y != NULL);
    assert(c != NULL);
    assert(!parser_finished(p));

    return parser_parse_u32(p, x) && parser_parse_ws(p) &&
           parser_parse_u32(p, y) && parser_parse_ws(p) &&
           parser_parse_u32(p, c) && parser_parse_end(p);
}
