#include <dragon/common.h>
#include <dragon/fnv1a.h>

bool u32_eq(const u32* a, const u32* b)
{
    return *a == *b;
}

size_t u32_hash(const u32* key)
{
    /*return FNV1a((const char*) key, 4);*/
    return ((size_t)*key);
}
